var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware');
require('dotenv').config();

var ordendetalle_controller = require('../controllers/ordendetalle_controller');

router.get('/',middleware.ensureAuthenticated, ordendetalle_controller.getAll);
router.get('/byFormat',middleware.ensureAuthenticated, ordendetalle_controller.getByFormat);
module.exports = router;

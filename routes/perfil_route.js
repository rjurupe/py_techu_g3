var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware');
require('dotenv').config();

var perfil_controller = require('../controllers/perfil_controller');

router.get('/',middleware.ensureAuthenticated, perfil_controller.getAllProfile);
router.get('/:id',middleware.ensureAuthenticated, perfil_controller.getByIdProfile);
router.get('/menu/:id',perfil_controller.getMenuByProfile);
module.exports = router;

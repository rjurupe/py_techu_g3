var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware');
require('dotenv').config();

var oficina_controller = require('../controllers/oficina_controller');

router.get('/',middleware.ensureAuthenticated, oficina_controller.getAllOffice);
router.get('/:id',middleware.ensureAuthenticated, oficina_controller.getByIdOffice);
module.exports = router;

var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware')
require('dotenv').config();

var orden_controller = require('../controllers/orden_controller');

router.get('/',middleware.ensureAuthenticated, orden_controller.getAllOrders);
router.get('/:id',middleware.ensureAuthenticated, orden_controller.getOrder);
router.post('/',middleware.ensureAuthenticated, orden_controller.insertOrder);
router.put('/',middleware.ensureAuthenticated, orden_controller.updateOrder);
router.delete('/:id',middleware.ensureAuthenticated, orden_controller.deleteOrder);
router.post('/filters/',middleware.ensureAuthenticated, orden_controller.getByFilterOrder);

module.exports = router;

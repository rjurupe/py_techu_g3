var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware');
require('dotenv').config();

var usuario_controller = require('../controllers/usuario_controller');
router.get('/',middleware.ensureAuthenticated,usuario_controller.getAllUsers);
router.post('/', usuario_controller.insertUser);
router.get('/:id',middleware.ensureAuthenticated,usuario_controller.getByIdUser);
router.put('/', middleware.ensureAuthenticated,usuario_controller.updateUser);
router.delete('/:id',middleware.ensureAuthenticated,usuario_controller.deleteUser);
router.post('/login', usuario_controller.login);
router.post('/logout', usuario_controller.logout);
router.put('/activate/:id', usuario_controller.activateUser);
router.put('/desactivate/:id', usuario_controller.desactivateUser);

module.exports = router;

var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware');
require('dotenv').config();

var tipotarjeta_controller = require('../controllers/tipotarjeta_controller');


router.get('/',middleware.ensureAuthenticated, tipotarjeta_controller.getAllCardType);
router.get('/:id',middleware.ensureAuthenticated, tipotarjeta_controller.getByIdCardType);
router.get('/',middleware.ensureAuthenticated, tipotarjeta_controller.getAllCardType);
router.get('/:id',middleware.ensureAuthenticated, tipotarjeta_controller.getByIdCardType);
router.get('/',middleware.ensureAuthenticated, tipotarjeta_controller.getAllCardType);
router.get('/:id',middleware.ensureAuthenticated, tipotarjeta_controller.getByIdCardType);
module.exports = router;

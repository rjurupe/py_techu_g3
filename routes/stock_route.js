var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware')
require('dotenv').config();

var stock_controller = require('../controllers/stock_controller');
router.get('/',middleware.ensureAuthenticated, stock_controller.getAllStock);
router.post('/filters',middleware.ensureAuthenticated, stock_controller.getByFilterStock);
module.exports = router;

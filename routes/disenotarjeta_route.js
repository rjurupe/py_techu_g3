var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware');
require('dotenv').config();

var disenotarjeta_controller = require('../controllers/disenotarjeta_controller');

router.get('/',middleware.ensureAuthenticated, disenotarjeta_controller.getAllCardDesign);
router.get('/:id',middleware.ensureAuthenticated, disenotarjeta_controller.getByIdCardDesign);
module.exports = router;

var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware');
require('dotenv').config();

var formatotarjeta_controller = require('../controllers/formatotarjeta_controller');

router.get('/',middleware.ensureAuthenticated, formatotarjeta_controller.getAllCardFormat);
router.get('/:id',middleware.ensureAuthenticated, formatotarjeta_controller.getByIdCardFormat);
module.exports = router;

var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware');
require('dotenv').config();

var ordencalculada_controller = require('../controllers/ordencalculada_controller');
router.get('/',middleware.ensureAuthenticated,ordencalculada_controller.getAllCalculatedOrders);
router.post('/', ordencalculada_controller.insertCalculatedOrder);
router.get('/:id',middleware.ensureAuthenticated,ordencalculada_controller.getByIdCalculatedOrder);
router.put('/', middleware.ensureAuthenticated,ordencalculada_controller.updateCalculatedOrder);
router.put('/updateQuantity/', middleware.ensureAuthenticated,ordencalculada_controller.updateQuantityCalculatedOrder);
router.delete('/:id',middleware.ensureAuthenticated,ordencalculada_controller.deleteCalculatedOrder);
router.put('/activate/:id', ordencalculada_controller.confirmCalculatedOrder);
router.put('/desactivate/:id', ordencalculada_controller.desactivateCalculatedOrder);
router.post('/filters/',middleware.ensureAuthenticated,ordencalculada_controller.getByFiltersCalculatedOrder);
module.exports = router;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrdenSchema = new Schema({
    name: {
            type: String,
            required: [true, 'Debe ingresar el nombre del producto'],
            max: 100
          },
    price: {
            type: Number
          }

});


// Export the model
module.exports = mongoose.model('Producto', ProductoSchema,'producto');

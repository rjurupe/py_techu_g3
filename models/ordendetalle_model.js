var mongoose = require('mongoose');

const OrdenDetalleSchema = mongoose.Schema({
      bin_tarjeta: {
        type: String,
        required: [true, 'Debe ingresar el bin_tarjeta']
      },
      nro_tarjeta: {
        type: String,
        required: [true, 'Debe ingresar nro_tarjeta']
      },
      oficina_solicitante: {
        type: String,
        required: [true, 'Debe ingresar el oficina']
      },
       nro_orden: {
        type: String,
        required: [true, 'Debe ingresar el nro orden']
      },
      tipo_tarjeta: {
        type: String,
        required: [true, 'Debe ingresar tipo tarjeta']
      },
      formato_tarjeta: {
        type: String,
        required: [true, 'Debe ingresar formato tarjeta']
      },
      fecha_vinculacion: {
        type: String
      },
      hora_vinculacion: {
        type: String
      },
      usuario_vinculacion: {
        type: String
      },
      motivo_baja: {
        type: String
      },
      fecha_baja: {
        type: String
      },
      hora_baja: {
        type: String
      },
      usuario_baja: {
        type: String
      },
      leyenda: {
        type: String
      },
      usuario_actualiza: {
        type: String
      },
      terminal_actualiza: {
        type: String
      },
      fecha_estampacion: {
        type: String
      },
      usuario_estampacion: {
        type: String
      }
  });

  // Export the model
  module.exports = mongoose.model('OrdenDetalle', OrdenDetalleSchema,'ordenes_detalle');

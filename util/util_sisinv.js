require('dotenv').config();
const WRITE_LOG = process.env.WRITE_LOG;

function printLog(message){
   if(WRITE_LOG=='1'){
       console.log(message);
   }
}

module.exports.printLog=printLog;
var jwt = require('jsonwebtoken');
var util_sisgein = require('./util_sisinv');
require('dotenv').config();
const JWT_SECRET_PASS=process.env.JWT_SECRET_PASS;
const JWT_VALIDATE_TOKEN=process.env.JWT_VALIDATE_TOKEN;

exports.ensureAuthenticated = function(req, res, next) {
    if(JWT_VALIDATE_TOKEN=='1'){
        var token = req.headers['authorization']
        console.log(token);
        if(!token) {
            return res
            .status(403)
            .send({mensaje: "Tu petición no tiene cabecera de autorización"});
        }
        jwt.verify(token, JWT_SECRET_PASS, function(err, user) {
            if (err) {
                      res.status(401).send({
                      mensaje: 'Token inválido'
                })
            } else {
                      next();
            }
        });
    }else{
        next();
    }
}

var express = require('express');
var usuarioRoute = require('./routes/usuario_route');
var perfilRoute = require('./routes/perfil_route');
var ordencalculadaRoute = require('./routes/ordencalculada_route');
var ordenRoute = require('./routes/orden_route');
var stockRoute = require('./routes/stock_route');
var oficinaRoute = require('./routes/oficina_route');
var tipotarjetaRoute = require('./routes/tipotarjeta_route');
var formatotarjetaRoute = require('./routes/formatotarjeta_route');
var disenotarjetaRoute = require('./routes/disenotarjeta_route');

//var ordendetalleRoute = require('./routes/ordendetalle_route');
var app = express();

var bodyParser = require("body-parser");
app.use(bodyParser.json());

// const DB_CONECTION_MONGOOSE = process.env.DB_CONECTION_MONGOOSE;
const URL_API_REST = process.env.URL_API_REST;
// const DEFAULT_PORT = 3000;
// const PORT = process.env.PORT || DEFAULT_PORT;
// require('dotenv').config();

const cors = require('cors');
app.use(cors());

app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({limit:'50mb',extended: true}));

//*****Lineas agregadas para que el NAVEGADOR no bloquee las peticiones al MLAB (otro dominio)
//*****por defecto*****//
app.options('*', cors());

// app.get('/',function(req,res){
// res.send('Hello World');
// });

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//Controllers
app.use(URL_API_REST+'/usuarios',usuarioRoute);
app.use(URL_API_REST+'/perfiles',perfilRoute);
app.use(URL_API_REST+'/ordenescalculadas',ordencalculadaRoute);
app.use(URL_API_REST+'/ordenes',ordenRoute);
app.use(URL_API_REST+'/stock',stockRoute);
app.use(URL_API_REST+'/oficinas',oficinaRoute);
app.use(URL_API_REST+'/tipos_tarjeta',tipotarjetaRoute);
app.use(URL_API_REST+'/formatos_tarjeta',formatotarjetaRoute);
app.use(URL_API_REST+'/disenos_tarjeta',disenotarjetaRoute);



// var mongoose = require('mongoose');
// mongoose.connect(DB_CONECTION_MONGOOSE, { useNewUrlParser: true });
// mongoose.Promise = global.Promise;
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.listen(3000,function(){
console.log('escuchando');
});

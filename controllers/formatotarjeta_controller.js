var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;
const moment = require('node-moment');

require('dotenv').config();


  exports.getAllCardFormat = function(req,res) {
  try{

      util_sisgein.printLog('Iniciando metodo getAllCardFormat ');

      var queryStringFormato = 'f={"_id":0}&';
      util_sisgein.printLog("[getAllCardFormat] " + queryStringFormato + API_KEY_MLAB);

      var httpClient = requestJSON.createClient(URL_API_MLAB);
      httpClient.get('formatos_tarjeta?' + queryStringFormato + API_KEY_MLAB,
      function(errformato, respuestaMLabformato, bodyformato) {
         var responseformato = {};
         if(errformato) {
             responseformato = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los formatos de tarjeta."}
             res.status(500);
         } else {
           if(bodyformato.length > 0) {
             res.status(200);
             console.log(bodyformato.length);
             responseformato = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":bodyformato.length,"formatostarjeta":bodyformato}
           } else {
             responseformato = {"codigo_mensaje":"00","mensaje" : "No existe formatos de etarjeta  a procesar"}
             res.status(201);
           }
         }
         res.send(responseformato);
       });
     }catch(ex){
         res.status(500);
         res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo getAllCardFormat ","detalle":ex+""});
     }
  };

  exports.getByIdCardFormat = function(req,res) {
  try{

    util_sisgein.printLog('Iniciando metodo getByIdCardFormat ');

    var id_formatotarjeta = req.params.id;
    console.log("[getByIdCardFormat]  id_formatotarjeta " + id_formatotarjeta);

    var queryStringFormato = 'q={"id_formatotarjeta":"'  + id_formatotarjeta + '"}&';
    console.log("queryString => " + queryStringFormato);
    var queryStrFieldFormato = 'f={"_id":0}&';

    var httpClient = requestJSON.createClient(URL_API_MLAB);
    httpClient.get('formatos_tarjeta?' + queryStringFormato + queryStrFieldFormato + API_KEY_MLAB,

    function(errformato, respuestaMLabformato, bodyformato) {
        var responseformato = {};
        if(errformato) {
            responseformato = {"codigo_mensaje":"00","mensaje" : "Error obteniendo datos formatos de tarjeta."}
            res.status(500);
        } else {
          if(bodyformato.length > 0) {
            console.log(bodyformato.length);

            responseformato = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":bodyformato.length,"formatostarjeta":bodyformato}
          } else {
            responseformato = {"codigo_mensaje":"00","mensaje" : "No existe el formato de tarjeta"}
            res.status(404);
          }
        }
        res.send(responseformato);
      });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo getByIdCardFormat ","detalle":ex+""});
    }

    };

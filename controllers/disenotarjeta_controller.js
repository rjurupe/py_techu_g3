var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;
const moment = require('node-moment');

require('dotenv').config();

    exports.getAllCardDesign = function(req,res) {
    try{

      util_sisgein.printLog('Iniciando metodo getAllCardDesign ');

       var queryStringDiseno = 'f={"_id":0}&';
       util_sisgein.printLog("[getAllCardDesign] " + queryStringDiseno + API_KEY_MLAB);

       var httpClientDiseno = requestJSON.createClient(URL_API_MLAB);
       httpClientDiseno.get('disenos_tarjeta?' + queryStringDiseno + API_KEY_MLAB,
       function(errdiseno, respuestaMLabdiseno, bodydiseno) {
           var responsediseno = {};
           if(errdiseno) {
               responsediseno = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los s de tarjeta."}
               res.status(500);
           } else {
             if(bodydiseno.length > 0) {
               res.status(200);
               console.log(bodydiseno.length);
               responsediseno = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":bodydiseno.length,"disenostarjeta":bodydiseno}
             } else {
               responsediseno = {"codigo_mensaje":"00","mensaje" : "No existe disenos d etarjeta  a procesar"}
               res.status(201);
             }
           }
           res.send(responsediseno);
         });
       }catch(ex){
           res.status(500);
           res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo getAllCardDesign ","detalle":ex+""});
       }
    };

    exports.getByIdCardDesign = function(req,res) {
    try{

      util_sisgein.printLog('Iniciando metodo getByIdCardDesign ');

      var diseno_tarjeta = req.params.id;
      console.log("[getByIdCardDesign]  diseno_tarjeta " + diseno_tarjeta);

      var queryStringDiseno = 'q={"id_disenotarjeta":"'  + diseno_tarjeta + '"}&';
      console.log("queryStringDiseno => " + queryStringDiseno);
      var queryStrFieldDiseno = 'f={"_id":0}&';

      var httpClientDiseno= requestJSON.createClient(URL_API_MLAB);
      httpClientDiseno.get('disenos_tarjeta?' + queryStringDiseno + queryStrFieldDiseno + API_KEY_MLAB,

      function(errdiseno, respuestaMLabdiseno, bodydiseno) {
          var responsediseno = {};
          if(errdiseno) {
              responsediseno = {"codigo_mensaje":"00","mensaje" : "Error obteniendo datos der la tarjeta."}
              res.status(500);
          } else {
            if(bodydiseno.length > 0) {
              console.log(bodydiseno.length);

              responsediseno = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":bodydiseno.length,"disenostarjeta":bodydiseno}
            } else {
              responsediseno = {"codigo_mensaje":"00","mensaje" : "No existe el diseno  de tarjeta"}
              res.status(404);
            }
          }
          res.send(responsediseno);
        });
      }catch(ex){
          res.status(500);
          res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo getByIdCardDesign ","detalle":ex+""});
      }

      };


var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;
const moment = require('node-moment');

require('dotenv').config();

exports.getAllStock = function (req, res) {
  try{
       var tipos_tarjeta={};
       var formatos_tarjeta={};
       var disenos_tarjeta={};
       var oficinas={};

           util_sisgein.printLog("Obtener todas las oficinas");
           var queryStringoficina = 'f={"_id":0}&';
           var httpClientOficina = requestJSON.createClient(URL_API_MLAB);
           httpClientOficina.get('oficinas?' + queryStringoficina + API_KEY_MLAB,
           function(erroficina, respuestaMLaboficina, bodyoficina) {
                     var responseoficinas = {};
                     if(erroficina) {
                         responseoficinas = {"codigo_mensaje":"00","mensaje" : "Error obteniendo las oficinas."}
                         res.status(500);
                         res.send(responseoficinas);
                     } else {
                             if(bodyoficina.length > 0) {
                               util_sisgein.printLog("Nro de oficinas: "+bodyoficina.length);
                               oficinas=bodyoficina;
//TIPO tarjeta

                              util_sisgein.printLog('Iniciando metodo getAllCardType ');
                               var queryStringTipo = 'f={"_id":0}&';
                               util_sisgein.printLog("[getAllCardType] " + queryStringTipo + API_KEY_MLAB);
                               var httpClient = requestJSON.createClient(URL_API_MLAB);
                               httpClient.get('tipos_tarjeta?' + queryStringTipo + API_KEY_MLAB,
                               function(errtipo, respuestaMLabtipo, bodytipo) {
                                   var responsetipo = {};
                                   if(errtipo) {
                                       responsetipo = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los tipos de tarjeta."}
                                       res.status(500);
                                   } else {
                                     if(bodytipo.length > 0) {
                                      util_sisgein.printLog("Nro de tipo de tarjeta: "+bodytipo.length);
                                       tipos_tarjeta=bodytipo;

//Formato Tarjeta
                          util_sisgein.printLog('Iniciando metodo getAllCardFormat ');
                          var queryStringFormato = 'f={"_id":0}&';
                          util_sisgein.printLog("[getAllCardFormat] " + queryStringFormato + API_KEY_MLAB);
                          var httpClient = requestJSON.createClient(URL_API_MLAB);
                          httpClient.get('formatos_tarjeta?' + queryStringFormato + API_KEY_MLAB,
                          function(errformato, respuestaMLabformato, bodyformato) {
                             var responseformato = {};
                             if(errformato) {
                                 responseformato = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los formatos de tarjeta."}
                                 res.status(500);
                             } else {
                               if(bodyformato.length > 0) {
                                 util_sisgein.printLog("Nro de formatos de tarjeta: "+bodyformato.length);
                                 formatos_tarjeta=bodyformato;
// Diseno Tarjeta
                           util_sisgein.printLog('Iniciando metodo getAllCardDesign ');
                           var queryStringDiseno = 'f={"_id":0}&';
                           util_sisgein.printLog("[getAllCardDesign] " + queryStringDiseno + API_KEY_MLAB);
                           var httpClientDiseno = requestJSON.createClient(URL_API_MLAB);
                           httpClientDiseno.get('disenos_tarjeta?' + queryStringDiseno + API_KEY_MLAB,
                           function(errdiseno, respuestaMLabdiseno, bodydiseno) {
                             util_sisgein.printLog('marca diseno ');
                               var responsediseno = {};
                               if(errdiseno) {
                                   responsediseno = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los s de tarjeta."}
                                   res.status(500);
                               } else {
                                 if(bodydiseno.length > 0) {
                                   console.log(bodydiseno.length);
                                   disenos_tarjeta=bodydiseno;
//Stock

var queryString = 'f={"_id":0}&';
var httpClient = requestJSON.createClient(URL_API_MLAB);
httpClient.get('stock?' + queryString + API_KEY_MLAB,
function(err, respuestaMLab, body) {
    var response = {};
    if(err) {
             response = {"codigo_mensaje":"01","mensaje" : "Error obteniendo el Stock."};
             res.status(500);
    } else {
      if(body.length > 0) {
              res.status(200);
              console.log(body.length);
              var arraystock=[];
              var stock={};
              arraystock=body;

              for (x=0;x<body.length;x++){
                stock = {};
                Object.keys(body[x]).forEach(key => stock[key] = body[x][key]);
                var t=0;
                var f=0;
                var d=0;
                var o=0;
                var encuentratipotarjeta=false;
                var encuentraformatotarjeta=false;
                var encuentradisenotarjeta=false;
                var encuentraoficina=false;
                var descripciontipotarjeta="";
                var descripcionformatotarjeta="";
                var descripciondisenotarjeta="";
                var descripcionoficina="";
                if (typeof stock["CODIGO_OFICINA"] !== 'undefined') {
                if (oficinas.length>=1){
                    while(o<oficinas.length && (!encuentraoficina)){
                      var oficina =oficinas[o];
                          if (typeof oficina["CODIGO_OFICINA"] !== 'undefined'){
                                if (stock["CODIGO_OFICINA"]==oficinas[o]["CODIGO_OFICINA"]){
                                    encuentraoficina=true;
                                    descripcionoficina=oficinas[o]["DESCRIPCION_OFICINA"];
                                }else {
                                  o++;
                                }
                          }else {
                            o++;
                          }
                    }
                }
                }

                util_sisgein.printLog("stock tipo tarjeta: "+ stock["TIPO_TARJETA"]+" indice:"+x);
                if (typeof stock["TIPO_TARJETA"] !== 'undefined') {
                if (tipos_tarjeta.length>=1){
                    while((!encuentratipotarjeta)&& (t<tipos_tarjeta.length)){
                          if (stock["TIPO_TARJETA"]==tipos_tarjeta[t]["tipo_tarjeta"])
                            {
                              encuentratipotarjeta=true;
                              descripciontipotarjeta=tipos_tarjeta[t]["desc_tipo"];
                            }else {
                              t++;
                            }
                    }
                }
                }

                if (typeof stock["FORMATO_TARJETA"] !== 'undefined') {
                if (formatos_tarjeta.length>=1){
                    while((!encuentraformatotarjeta) && (f<formatos_tarjeta.length)){
                        //  util_sisgein.printLog("formato tarjeta: "+ formatos_tarjeta[f]["id_formatotarjeta"]+" indicef:"+f);
                          if (stock["FORMATO_TARJETA"]==formatos_tarjeta[f]["id_formatotarjeta"]){
                              encuentraformatotarjeta=true;
                              descripcionformatotarjeta=formatos_tarjeta[f]["descripcion_formatotarjeta"];
                              util_sisgein.printLog("descripcionformatotarjeta: "+ formatos_tarjeta[f]["descripcion_formatotarjeta"]+" encontrado:"+f);
                          }else {
                            f++;
                          }
                    }
                }
                }

                util_sisgein.printLog("stock diseno tarjeta: "+ stock["DISENO_TARJETA"]+" indice:"+x);
                if (typeof stock["DISENO_TARJETA"] !== 'undefined') {
                if (disenos_tarjeta.length>=1){
                    while((!encuentradisenotarjeta ) && (d<disenos_tarjeta.length)){
                //          util_sisgein.printLog("stock diseno tarjeta: "+ disenos_tarjeta[d]["id_disenotarjeta"]+" indiced:"+d);
                          if (stock["DISENO_TARJETA"]==disenos_tarjeta[d]["id_disenotarjeta"])
                            {
                              encuentradisenotarjeta=true;
                              descripciondisenotarjeta=disenos_tarjeta[d]["descripcion_disenotarjeta"];
                            }else {
                              d++;
                            }
                    }
                }
                }

                stock["descripciontipotarjeta"]=descripciontipotarjeta;
                stock["descripcionformatotarjeta"]=descripcionformatotarjeta;
                stock["descripciondisenotarjeta"]=descripciondisenotarjeta;
                stock["descripcionoficina"]=descripcionoficina;
                arraystock[x]=stock;
              }

              response = {"codigo_mensaje":"01","mensaje" : "Listado exitoso'.","numero_elementos":+body.length,"stock":arraystock}
      } else {
             response = {"codigo_mensaje":"01","mensaje" : "No existe datos de Stock"}
             res.status(201);
      }
    }
    res.send(response);
  });

//Stock
                                 } else {
                                   responsediseno = {"codigo_mensaje":"00","mensaje" : "No existe disenos d etarjeta  a procesar"}
                                   res.status(404);
                                    res.send(responsediseno);
                                 }
                               }

                             });
// Diseno Tarjeta
                               } else {
                                 responseformato = {"codigo_mensaje":"00","mensaje" : "No existe formatos de etarjeta  a procesar"}
                                 res.status(404);
                                 res.send(responseformato);
                               }
                             }

                           });


//Formato Tarjeta
                                     } else {
                                       responsetipo = {"codigo_mensaje":"00","mensaje" : "No existe tipos d etarjeta  a procesar"}
                                       res.status(404);
                                       res.send(responsetipo);
                                     }
                                   }

                                 });

//TIPO TARJETA


                             } else {
                               responseoficinas = {"codigo_mensaje":"00","mensaje" : "No existe oficinas a listar"}
                               res.status(404);
                               res.send(responseoficinas);
                             }
                     }

             });




   }catch(ex){
       res.status(500);
       res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo stock.getAllStock","detalle":ex+""});
   }
 };



exports.getByFilterStock = function (req, res) {
    try{

      var tipos_tarjeta={};
      var formatos_tarjeta={};
      var disenos_tarjeta={};
      var oficinas={};

          util_sisgein.printLog("Obtener todas las oficinas");
          var queryStringoficina = 'f={"_id":0}&';
          var httpClientOficina = requestJSON.createClient(URL_API_MLAB);
          httpClientOficina.get('oficinas?' + queryStringoficina + API_KEY_MLAB,
          function(erroficina, respuestaMLaboficina, bodyoficina) {
                    var responseoficinas = {};
                    if(erroficina) {
                        responseoficinas = {"codigo_mensaje":"00","mensaje" : "Error obteniendo las oficinas."}
                        res.status(500);
                        res.send(responseoficinas);
                    } else {
                            if(bodyoficina.length > 0) {
                              util_sisgein.printLog("Nro de oficinas: "+bodyoficina.length);
                              oficinas=bodyoficina;
 //TIPO tarjeta

                             util_sisgein.printLog('Iniciando metodo getAllCardType ');
                              var queryStringTipo = 'f={"_id":0}&';
                              util_sisgein.printLog("[getAllCardType] " + queryStringTipo + API_KEY_MLAB);
                              var httpClient = requestJSON.createClient(URL_API_MLAB);
                              httpClient.get('tipos_tarjeta?' + queryStringTipo + API_KEY_MLAB,
                              function(errtipo, respuestaMLabtipo, bodytipo) {
                                  var responsetipo = {};
                                  if(errtipo) {
                                      responsetipo = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los tipos de tarjeta."}
                                      res.status(500);
                                  } else {
                                    if(bodytipo.length > 0) {
                                     util_sisgein.printLog("Nro de tipo de tarjeta: "+bodytipo.length);
                                      tipos_tarjeta=bodytipo;

 //Formato Tarjeta
                         util_sisgein.printLog('Iniciando metodo getAllCardFormat ');
                         var queryStringFormato = 'f={"_id":0}&';
                         util_sisgein.printLog("[getAllCardFormat] " + queryStringFormato + API_KEY_MLAB);
                         var httpClient = requestJSON.createClient(URL_API_MLAB);
                         httpClient.get('formatos_tarjeta?' + queryStringFormato + API_KEY_MLAB,
                         function(errformato, respuestaMLabformato, bodyformato) {
                            var responseformato = {};
                            if(errformato) {
                                responseformato = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los formatos de tarjeta."}
                                res.status(500);
                            } else {
                              if(bodyformato.length > 0) {
                                util_sisgein.printLog("Nro de formatos de tarjeta: "+bodyformato.length);
                                formatos_tarjeta=bodyformato;
 // Diseno Tarjeta
                          util_sisgein.printLog('Iniciando metodo getAllCardDesign ');
                          var queryStringDiseno = 'f={"_id":0}&';
                          util_sisgein.printLog("[getAllCardDesign] " + queryStringDiseno + API_KEY_MLAB);
                          var httpClientDiseno = requestJSON.createClient(URL_API_MLAB);
                          httpClientDiseno.get('disenos_tarjeta?' + queryStringDiseno + API_KEY_MLAB,
                          function(errdiseno, respuestaMLabdiseno, bodydiseno) {
                            util_sisgein.printLog('marca diseno ');
                              var responsediseno = {};
                              if(errdiseno) {
                                  responsediseno = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los s de tarjeta."}
                                  res.status(500);
                              } else {
                                if(bodydiseno.length > 0) {
                                  console.log(bodydiseno.length);
                                  disenos_tarjeta=bodydiseno;

  //Filtros Stock

        util_sisgein.printLog("Obtener stock por filtros");
        var filtro_oficina_solicitante=req.body.filtro_oficina_solicitante;
        var filtro_tipo_tarjeta=req.body.filtro_tipo_tarjeta;
        var filtro_formato_tarjeta=req.body.filtro_formato_tarjeta;
        var filtro_diseno_tarjeta=req.body.filtro_diseno_tarjeta;
        var filtro_estado=req.body.filtro_estado;
        var filtro_stock_inicio=req.body.filtro_stock_inicio;
        var filtro_stock_fin=req.body.filtro_stock_fin;

        var queryString='q={';
        var contador=0;

        if (filtro_oficina_solicitante=="" && filtro_tipo_tarjeta==""
            && filtro_formato_tarjeta=="" && filtro_diseno_tarjeta==""
            //&& filtro_stock_inicio=="0" && filtro_stock_fin=="0"
            && filtro_estado=="" ){
              res.status(200);


//INICIO GENERAL

var queryString = 'f={"_id":0}&';
var httpClient = requestJSON.createClient(URL_API_MLAB);
httpClient.get('stock?' + queryString + API_KEY_MLAB,
function(err, respuestaMLab, body) {
    var response = {};
    if(err) {
             response = {"codigo_mensaje":"01","mensaje" : "Error obteniendo el Stock."};
             res.status(500);
    } else {
      if(body.length > 0) {
              res.status(200);

              var arraystock=[];
              var stock={};
              arraystock=body;

              for (x=0;x<body.length;x++){
                stock = {};
                Object.keys(body[x]).forEach(key => stock[key] = body[x][key]);
                var t=0;
                var f=0;
                var d=0;
                var o=0;
                var encuentratipotarjeta=false;
                var encuentraformatotarjeta=false;
                var encuentradisenotarjeta=false;
                var encuentraoficina=false;
                var descripciontipotarjeta="";
                var descripcionformatotarjeta="";
                var descripciondisenotarjeta="";
                var descripcionoficina="";
                if (typeof stock["CODIGO_OFICINA"] !== 'undefined') {
                if (oficinas.length>=1){
                    while(o<oficinas.length && (!encuentraoficina)){
                      var oficina =oficinas[o];
                          if (typeof oficina["CODIGO_OFICINA"] !== 'undefined'){
                                if (stock["CODIGO_OFICINA"]==oficinas[o]["CODIGO_OFICINA"]){
                                    encuentraoficina=true;
                                    descripcionoficina=oficinas[o]["DESCRIPCION_OFICINA"];
                                }else {
                                  o++;
                                }
                          }else {
                            o++;
                          }
                    }
                }
                }

                util_sisgein.printLog("stock tipo tarjeta: "+ stock["TIPO_TARJETA"]+" indice:"+x);
                if (typeof stock["TIPO_TARJETA"] !== 'undefined') {
                if (tipos_tarjeta.length>=1){
                    while((!encuentratipotarjeta)&& (t<tipos_tarjeta.length)){
                          if (stock["TIPO_TARJETA"]==tipos_tarjeta[t]["tipo_tarjeta"])
                            {
                              encuentratipotarjeta=true;
                              descripciontipotarjeta=tipos_tarjeta[t]["desc_tipo"];
                            }else {
                              t++;
                            }
                    }
                }
                }

                if (typeof stock["FORMATO_TARJETA"] !== 'undefined') {
                if (formatos_tarjeta.length>=1){
                    while((!encuentraformatotarjeta) && (f<formatos_tarjeta.length)){
                        //  util_sisgein.printLog("formato tarjeta: "+ formatos_tarjeta[f]["id_formatotarjeta"]+" indicef:"+f);
                          if (stock["FORMATO_TARJETA"]==formatos_tarjeta[f]["id_formatotarjeta"]){
                              encuentraformatotarjeta=true;
                              descripcionformatotarjeta=formatos_tarjeta[f]["descripcion_formatotarjeta"];
                              util_sisgein.printLog("descripcionformatotarjeta: "+ formatos_tarjeta[f]["descripcion_formatotarjeta"]+" encontrado:"+f);
                          }else {
                            f++;
                          }
                    }
                }
                }

                util_sisgein.printLog("stock diseno tarjeta: "+ stock["DISENO_TARJETA"]+" indice:"+x);
                if (typeof stock["DISENO_TARJETA"] !== 'undefined') {
                if (disenos_tarjeta.length>=1){
                    while((!encuentradisenotarjeta ) && (d<disenos_tarjeta.length)){
                //          util_sisgein.printLog("stock diseno tarjeta: "+ disenos_tarjeta[d]["id_disenotarjeta"]+" indiced:"+d);
                          if (stock["DISENO_TARJETA"]==disenos_tarjeta[d]["id_disenotarjeta"])
                            {
                              encuentradisenotarjeta=true;
                              descripciondisenotarjeta=disenos_tarjeta[d]["descripcion_disenotarjeta"];
                            }else {
                              d++;
                            }
                    }
                }
                }

                stock["descripciontipotarjeta"]=descripciontipotarjeta;
                stock["descripcionformatotarjeta"]=descripcionformatotarjeta;
                stock["descripciondisenotarjeta"]=descripciondisenotarjeta;
                stock["descripcionoficina"]=descripcionoficina;
                arraystock[x]=stock;
              }

              response = {"codigo_mensaje":"01","mensaje" : "Listado exitoso'.","numero_elementos":+body.length,"stock":arraystock}
      } else {
             response = {"codigo_mensaje":"01","mensaje" : "No existe datos de Stock"}
             res.status(201);
      }
    }
    res.send(response);
  });

//FIN GENERAL


        }else{
              if (filtro_oficina_solicitante!=""){
                  queryString=queryString+'"CODIGO_OFICINA":"'+filtro_oficina_solicitante+'"';
              }
              if (filtro_tipo_tarjeta!=""){
                  if (filtro_oficina_solicitante!="")
                      {queryString=queryString+",";}
                  queryString=queryString+'"TIPO_TARJETA":"'+filtro_tipo_tarjeta+'"';
              }
              if (filtro_formato_tarjeta!=""){
                  if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="")
                    {queryString=queryString+",";}
                  queryString=queryString+'"FORMATO_TARJETA":"'+filtro_formato_tarjeta+'"';
              }
              if (filtro_diseno_tarjeta!=""){
                  if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="" ||filtro_formato_tarjeta!="")
                    {queryString=queryString+",";}
                    queryString=queryString+'"DISENO_TARJETA":"'+filtro_diseno_tarjeta+'"';
              }

              if (filtro_estado!="" && filtro_stock_inicio!="" && filtro_stock_fin!=""){
                  if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="" ||filtro_formato_tarjeta!=""||filtro_diseno_tarjeta!="" )
                  {queryString=queryString+",";}

                  if(filtro_estado='pendiente'){
                        queryString=queryString+'"STOCK_PENDIENTE":{"$gte":'+filtro_stock_inicio+',"$lte":'+filtro_stock_fin+'}';
                  }else{
                        if (filtro_estado='transito'){
                              queryString=queryString+'"STOCK_TRANSITO":{"$gte":'+filtro_stock_inicio+',"$lte":'+filtro_stock_fin+'}';
                        }else{
                              if (filtro_estado='oficina'){
                                queryString=queryString+'"STOCK_OFICINA":{"$gte":'+filtro_stock_inicio+',"$lte":'+filtro_stock_fin+'}';
                              }
                        }
                  }
            }
            //queryString=queryString+'"fchproceso":{"$gte":{"$date":"'+ filtro_fecha_inicio+'"},"$lte":{"$date":"'+filtro_fecha_fin+'"}}';
            //q={"fechac":{"$gte":{"$date":"2019-12-04T00:00:50Z"},"$lte":{"$date":"2019-12-04T23:23:50Z"}}}
            //  }

                queryString=queryString+'}&';
                util_sisgein.printLog("queryString:"+queryString);
                var queryStrField='f={"_id":0}&';
                util_sisgein.printLog('stock?' + queryString +queryStrField +API_KEY_MLAB);
                var httpClient = requestJSON.createClient(URL_API_MLAB);
                httpClient.get('stock?' + queryString +queryStrField + API_KEY_MLAB,
                function(err, respuestaMLab, body) {
                       var response = {}
                       if(err) {
                         res.status(500);
                         response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo stock."}
                       } else {
                                if(body.length > 0) {
                                        res.status(200);
                                        util_sisgein.printLog("items: "+body.length);
//INICIO NUEVO

var arraystock=[];
var stock={};
arraystock=body;

for (x=0;x<body.length;x++){
  stock = {};
  Object.keys(body[x]).forEach(key => stock[key] = body[x][key]);
  var t=0;
  var f=0;
  var d=0;
  var o=0;
  var encuentratipotarjeta=false;
  var encuentraformatotarjeta=false;
  var encuentradisenotarjeta=false;
  var encuentraoficina=false;
  var descripciontipotarjeta="";
  var descripcionformatotarjeta="";
  var descripciondisenotarjeta="";
  var descripcionoficina="";
  if (typeof stock["CODIGO_OFICINA"] !== 'undefined') {
  if (oficinas.length>=1){
      while(o<oficinas.length && (!encuentraoficina)){
        var oficina =oficinas[o];
            if (typeof oficina["CODIGO_OFICINA"] !== 'undefined'){
                  if (stock["CODIGO_OFICINA"]==oficinas[o]["CODIGO_OFICINA"]){
                      encuentraoficina=true;
                      descripcionoficina=oficinas[o]["DESCRIPCION_OFICINA"];
                  }else {
                    o++;
                  }
            }else {
              o++;
            }
      }
  }
  }

  util_sisgein.printLog("stock tipo tarjeta: "+ stock["TIPO_TARJETA"]+" indice:"+x);
  if (typeof stock["TIPO_TARJETA"] !== 'undefined') {
  if (tipos_tarjeta.length>=1){
      while((!encuentratipotarjeta)&& (t<tipos_tarjeta.length)){
            if (stock["TIPO_TARJETA"]==tipos_tarjeta[t]["tipo_tarjeta"])
              {
                encuentratipotarjeta=true;
                descripciontipotarjeta=tipos_tarjeta[t]["desc_tipo"];
              }else {
                t++;
              }
      }
  }
  }

  if (typeof stock["FORMATO_TARJETA"] !== 'undefined') {
  if (formatos_tarjeta.length>=1){
      while((!encuentraformatotarjeta) && (f<formatos_tarjeta.length)){
          //  util_sisgein.printLog("formato tarjeta: "+ formatos_tarjeta[f]["id_formatotarjeta"]+" indicef:"+f);
            if (stock["FORMATO_TARJETA"]==formatos_tarjeta[f]["id_formatotarjeta"]){
                encuentraformatotarjeta=true;
                descripcionformatotarjeta=formatos_tarjeta[f]["descripcion_formatotarjeta"];
                util_sisgein.printLog("descripcionformatotarjeta: "+ formatos_tarjeta[f]["descripcion_formatotarjeta"]+" encontrado:"+f);
            }else {
              f++;
            }
      }
  }
  }

  util_sisgein.printLog("stock diseno tarjeta: "+ stock["DISENO_TARJETA"]+" indice:"+x);
  if (typeof stock["DISENO_TARJETA"] !== 'undefined') {
  if (disenos_tarjeta.length>=1){
      while((!encuentradisenotarjeta ) && (d<disenos_tarjeta.length)){
  //          util_sisgein.printLog("stock diseno tarjeta: "+ disenos_tarjeta[d]["id_disenotarjeta"]+" indiced:"+d);
            if (stock["DISENO_TARJETA"]==disenos_tarjeta[d]["id_disenotarjeta"])
              {
                encuentradisenotarjeta=true;
                descripciondisenotarjeta=disenos_tarjeta[d]["descripcion_disenotarjeta"];
              }else {
                d++;
              }
      }
  }
  }

  stock["descripciontipotarjeta"]=descripciontipotarjeta;
  stock["descripcionformatotarjeta"]=descripcionformatotarjeta;
  stock["descripciondisenotarjeta"]=descripciondisenotarjeta;
  stock["descripcionoficina"]=descripcionoficina;
  arraystock[x]=stock;
}


//FIN NUEVO
                                        response = {"codigo_mensaje":"01","mensaje" : "Listado exitoso'.","numero_elementos":+body.length,"stock":arraystock}

                                } else {
                                        res.status(201);
                                        response = {"codigo_mensaje":"00","mensaje" : "Ningún elemento 'stock'."}
                                }
                      }
                      res.send(response);
                });
        }


//Fitros Stock

      } else {
        responsediseno = {"codigo_mensaje":"00","mensaje" : "No existe disenos d etarjeta  a procesar"}
        res.status(404);
         res.send(responsediseno);
      }
    }

  });
// Diseno Tarjeta
    } else {
      responseformato = {"codigo_mensaje":"00","mensaje" : "No existe formatos de etarjeta  a procesar"}
      res.status(404);
      res.send(responseformato);
    }
  }

});


//Formato Tarjeta
          } else {
            responsetipo = {"codigo_mensaje":"00","mensaje" : "No existe tipos d etarjeta  a procesar"}
            res.status(404);
            res.send(responsetipo);
          }
        }

      });

//TIPO TARJETA


  } else {
    responseoficinas = {"codigo_mensaje":"00","mensaje" : "No existe oficinas a listar"}
    res.status(404);
    res.send(responseoficinas);
  }
}

});


    }catch(ex){
        res.status(500);
       res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo stock.getByFilterStock","detalle":ex+""});
    }
};

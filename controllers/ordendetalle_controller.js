var OrdenDetalle = require('../models/ordendetalle_model');
var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;

require('dotenv').config();
//Obtener usuario por Id
exports.getAll = function (req, res) {
    try{
      console.log("entroooooooooooooooooooo");
        OrdenDetalle.find({}).exec(function (err, ordendetalle) {
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio un error al obtener los usuarios ","detalle":err.message});
            }else{
                res.status(200);
                res.send({"DetalleOrden":ordendetalle});
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.getAll","detalle":ex+""});
    }
};

exports.getByFormat = function(req,res){
  try{

    const aggregate = OrdenDetalle.aggregate([
    {
      "$group" : {
       _id:{
          "oficina_solicitante":"$oficina_solicitante",
          "formato_tarjeta":"$formato_tarjeta",
          "tipo_tarjeta":"$tipo_tarjeta"
       },
       count:{$sum:1}

     }
    }
  ]);

  /*const aggregate = OrdenDetalle.aggregate([
  {
   "$match": {
     "fecha_vinculacion": {$ne:"0001-01-01"}
   }
  }
  ]);
  */

  aggregate.exec(function (err, data) {
        if (err) {
            res.status(400);
            res.send({"mensaje":"Ocurrio un error al obtener los usuarios ","detalle":err.message});
        }else{
            res.status(200);
            res.send({"data":data});
        }
    });


  }catch(ex){
      res.status(500);
      res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.getAll","detalle":ex+""});
  }
}

var OrdenDetalle = require('../models/ordendetalle_model');
var util_sisgein = require('../util/util_sisinv');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;

require('dotenv').config();

//Obtener todos los perfiles
exports.getAllProfile = function (req, res) {
    try{
        util_sisgein.printLog("Obtener todos los perfiles: getAll");
        var httpClient = requestJSON.createClient(URL_API_MLAB);
        var queryString = 'f={"_id":0}&';
        httpClient.get('perfiles?' + queryString + API_KEY_MLAB,
        function(err, respuestaMLab, body) {
                 var response = {};
                 if(err){
                         res.status(400);
                         response = {"mensaje" : "Error obteniendo perfiles.","detalle":err.message};
                 }else  {
                         if(body.length > 0) {
                              res.status(200);
                              util_sisgein.printLog("Nro de perfiles: "+body.length);
                              response = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":body.length,"perfiles":body}
                              //response = body;
                         }else{
                              res.status(201);
                              response = {"mensaje" : "La lista de perfiles esta vacía."}
                         }
                 }
                res.send(response);
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.getAll","detalle":ex+""});
    }
};

//Obtener perfil  por Id
exports.getByIdProfile = function (req, res) {
    try{
        util_sisgein.printLog("Obtener perfil por id:"+ req.params.id);
        var idperfil=req.params.id;
        var queryString='q={"idperfil":'+idperfil+'}&';
        var queryStrField='f={"_id":0}&';
        var limFilter='l=1&';
        util_sisgein.printLog('perfiles?' + queryString +queryStrField +limFilter+ API_KEY_MLAB);
        var httpClient = requestJSON.createClient(URL_API_MLAB);
        httpClient.get('perfiles?' + queryString +queryStrField +limFilter+ API_KEY_MLAB,
        function(err, respuestaMLab, body) {
                 var response = {};
                 if(err) {
                   response = {"mensaje" : "Error obteniendo perfil."}
                   res.status(500);
                 } else {
                    util_sisgein.printLog('body'+ JSON.stringify(body));
                          if(body.length > 0) {
                            util_sisgein.printLog(body.length);
                            response = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":body.length,"perfiles":body}

                            //response = body;
                          } else {
                              response = {"codigo_mensaje":"00","mensaje" : "Ningún elemento 'perfil'."}
                              res.status(404);
                          }
                }
                res.send(response);
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo getByIdProfile","detalle":ex+""});
    }
};

//users consumiendo API REST de mLab
exports.getMenuByProfile = function (req, res) {
  try{
   util_sisgein.printLog("Obtener opciones del menu por perfil:"+ req.params.id);
   var id = req.params.id;
   var contador = 0;

   var httpClient = requestJSON.createClient(URL_API_MLAB);
   var queryStringID = 'q={"idperfil":' + id + '}&';

   httpClient.get('perfil_menu?' + queryStringID + API_KEY_MLAB,
   function(err, respuestaMLab, body) {
       var response = {};
       if(err || body[0] == undefined) {
           response = {"msg" : "Error obteniendo el perfil."}
           res.status(404);
           res.send(response);
       }else{
             var queryString = 'f={"_id":0, "idopcion":0,"secuencia":0,"estado":0,"fchcreacion":0,"hrscreacion":0}&s={"secuencia":1}&q={"idopcion":{"$in":[' + body[0].opcion + ']}}&';
             httpClient.get('opciones_menu?' + queryString + API_KEY_MLAB,
             function(err2, respuestaMLab2, body2) {
                     if(err2) {
                       response = {"msg" : "Error obteniendo opciones de menu."}
                       res.status(404);
                     }else{
                       response = body2;
                       res.status(200);
                     }
                     res.send(response);
              });
       }
   });
 }catch(ex){
     res.status(500);
     res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo getMenuByProfile","detalle":ex+""});
 }
};

var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;
const moment = require('node-moment');

require('dotenv').config();

exports.getAllCardType = function(req,res) {
try{

  util_sisgein.printLog('Iniciando metodo getAllCardType ');

   var queryStringTipo = 'f={"_id":0}&';
   util_sisgein.printLog("[getAllCardType] " + queryStringTipo + API_KEY_MLAB);

   var httpClient = requestJSON.createClient(URL_API_MLAB);
   httpClient.get('tipos_tarjeta?' + queryStringTipo + API_KEY_MLAB,
   function(errtipo, respuestaMLabtipo, bodytipo) {
       var responsetipo = {};
       if(errtipo) {
           responsetipo = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los tipos de tarjeta."}
           res.status(500);
       } else {
         if(bodytipo.length > 0) {
           res.status(200);
           console.log(bodytipo.length);
           responsetipo = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":bodytipo.length,"tipostarjeta":bodytipo}
         } else {
           responsetipo = {"codigo_mensaje":"00","mensaje" : "No existe tipos d etarjeta  a procesar"}
           res.status(201);
         }
       }
       res.send(responsetipo);
     });
   }catch(ex){
       res.status(500);
       res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo getAllCardType ","detalle":ex+""});
   }
};

exports.getByIdCardType = function(req,res) {
try{

  util_sisgein.printLog('Iniciando metodo getByIdCardType ');

  var tipo_tarjeta = req.params.id;
  console.log("[getByIdCardType]  tipo_tarjeta " + tipo_tarjeta);


  var queryStringTipo = 'q={"tipo_tarjeta":"'  + tipo_tarjeta + '"}&';
  console.log("queryString => " + queryStringTipo);
  var queryStrFieldTipo = 'f={"_id":0}&';

  var httpClientTipo = requestJSON.createClient(URL_API_MLAB);
  httpClientTipo.get('tipos_tarjeta?' + queryStringTipo + queryStrFieldTipo + API_KEY_MLAB,

  function(errTipo, respuestaMLabTipo, bodytipo) {
      var responseTipo = {};
      if(errTipo) {
          responseTipo = {"codigo_mensaje":"00","mensaje" : "Error obteniendo datos der la tarjeta."}
          res.status(500);
      } else {
        if(bodytipo.length > 0) {
          console.log(bodytipo.length);
          responseTipo = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":bodytipo.length,"tipostarjeta":bodytipo}
        } else {
          responseTipo = {"codigo_mensaje":"00","mensaje" : "No existe el tipo de tarjeta"}
          res.status(201);
        }
      }
      res.send(responseTipo);
    });
  }catch(ex){
      res.status(500);
      res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo getByIdCardType ","detalle":ex+""});
  }

  };

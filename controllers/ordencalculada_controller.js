var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');
const moment = require('node-moment');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;

require('dotenv').config();

//Obtener todas las ordenes

exports.getAllCalculatedOrders = function (req, res) {
    try{
        util_sisgein.printLog("Obtener todas las ordenes calculadas: getAll");
        var httpClient = requestJSON.createClient(URL_API_MLAB);
        var queryString = 'f={"_id":0}&';
        httpClient.get('ordenescalculadas?' + queryString + API_KEY_MLAB,
        function(err, respuestaMLab, body) {
                 var response = {};
                 if(err){
                         res.status(400);
                         response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo ordenes calculadas por confirmar.","detalle":err.message};
                 }else  {
                         if(body.length > 0) {
                              res.status(200);
                              util_sisgein.printLog("Nro de ordenes calculadas por confirmar: "+body.length);
                              response = {"codigo_mensaje":"01","mensaje" : "La lista de ordenes calculadas se obtuvo satisfactoriamente.","numero_elementos":body.length,"ordenescalculadas":body}

                         }else{
                              res.status(201);
                              response = {"codigo_mensaje":"00","mensaje" : "La lista de ordenes calculadas  esta vacía."}
                         }
                 }
                res.send(response);
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo ordencalculada.getAll","detalle":ex+""});
    }
};

exports.insertCalculatedOrder = function (req, res) {
    try{
          util_sisgein.printLog("Crear Orden Calculada");
          var httpClient = requestJSON.createClient(URL_API_MLAB);
          httpClient.get('ordenescalculadas?'+ API_KEY_MLAB,
          function(err, respuestaMLab, body) {
              if(err){
                      res.status(400);
                      res.send({"codigo_mensaje":"00","mensaje":"Ocurrio un error al obtener las ordenes calculadas ","detalle":err.message});
              }else{
                      var newId=body.length +1;
                      var fecha=new Date();
                      var fchcreacion=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                      var hrscreacion=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
                      util_sisgein.printLog("new Id:"+newId);
                      util_sisgein.printLog("fecha:"+fecha);
                      util_sisgein.printLog("fchcreacion:"+fchcreacion);
                      util_sisgein.printLog("hrscreacion:"+hrscreacion);
                      var newOrdenCalculada = {
                        'idorden' : newId,
                        'oficina_solicitante' : req.body.oficina_solicitante,
                        'tipo_tarjeta': req.body.tipo_tarjeta,
                        'formato_tarjeta': req.body.formato_tarjeta,
                        'diseno_tarjeta':req.body.diseno_tarjeta,
                        'can_tarjetasord':req.body.can_tarjetasord,
                        'can_tarjetascon':0,
                        'stock_actual':req.body.stock_actual,
                        'estado' : 1,
                        'fchproceso' :{"$date": moment(fchcreacion,"DD-MM-YYYY").toISOString()},
                        'hrsproceso' : hrscreacion,
                        'fchauditoria': fchcreacion,
                        'hrsauditoria': hrscreacion
                      };
                      httpClient.post(URL_API_MLAB+'ordenescalculadas?'+API_KEY_MLAB,newOrdenCalculada,
                      function (err,respuestaMLab,body) {
                          if (err){
                                    res.status(400);
                                    res.send({"codigo_mensaje":"00","mensaje":"Ocurrio un error al registrar el calculo de la orden ","detalle":err.message});
                          }else{
                                    res.status(200);
                                    if(body!=null){
                                       res.send({"codigo_mensaje":"01","mensaje":"orden calculada registrada satisfactoriamente","ordencalculada":newOrdenCalculada});
                                    }else{
                                       res.send({"codigo_mensaje":"00","mensaje":"Ocurrio un error al registrar el calculo de la orden"});
                                    }
                          }
                      });
              }
          });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo ordencalculada.Create","detalle":ex+""});
    }
};

//Obtener orden calculada  por Id
exports.getByIdCalculatedOrder = function (req, res) {
    try{
        util_sisgein.printLog("Obtener orden calculada por Id:"+ req.params.id);
        var idorden=req.params.id;
        var queryString='q={"idorden":'+idorden+'}&';
        var queryStrField='f={"_id":0}&';
        var limFilter='l=1&';
        util_sisgein.printLog('ordenescalculadas?' + queryString +queryStrField +limFilter+ API_KEY_MLAB);
        var httpClient = requestJSON.createClient(URL_API_MLAB);
        httpClient.get('ordenescalculadas?' + queryString +queryStrField +limFilter+ API_KEY_MLAB,
        function(err, respuestaMLab, body) {
                 var response = {};
                 if(err) {
                   res.status(500);
                   response = {"codigo_mensaje":"0","mensaje" : "Error obteniendo calculo de orden."}
                 } else {
                          //util_sisgein.printLog('body'+ JSON.stringify(body));
                          if(body.length > 0) {
                            res.status(200);
                            util_sisgein.printLog('"Numero de elementos":'+body.length+'""');
                            response = {"codigo_mensaje":"01","mensaje" : "La lista de ordenes calculadas se cargò satisfactoriamente.","numero_elementos":body.length,"ordencalculada":body};

                          } else {
                              res.status(201);
                              response = {"codigo_mensaje":"00","mensaje" : "Ningún elemento 'orden calculada'."}
                          }
                }
                res.send(response);
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo ordencalculada.getAll","detalle":ex+""});
    }
};


exports.getByFiltersCalculatedOrder = function (req, res) {
    try{

        util_sisgein.printLog("Obtener orden calculada por filtros");

        var filtro_oficina_solicitante=req.body.filtro_oficina_solicitante;
        var filtro_tipo_tarjeta=req.body.filtro_tipo_tarjeta;
        var filtro_formato_tarjeta=req.body.filtro_formato_tarjeta;
        var filtro_diseno_tarjeta=req.body.filtro_diseno_tarjeta;
        var filtro_estado=req.body.filtro_estado;
        var filtro_fecha_inicio=req.body.filtro_fecha_inicio;
        var filtro_fecha_fin=req.body.filtro_fecha_fin;

        var queryString='q={';
        var contador=0;

      if (filtro_oficina_solicitante=="" && filtro_tipo_tarjeta==""
          && filtro_formato_tarjeta=="" && filtro_diseno_tarjeta=="" && filtro_fecha_inicio==""
          && filtro_fecha_fin=="" &&  filtro_estado==""){
            res.status(404);
            res.send({"codigo_mensaje":"00","mensaje":"Se debe ingresar al menos un filtro"});
          }
      else{
        if (filtro_oficina_solicitante!=""){
            queryString=queryString+'"oficina_solicitante":"'+filtro_oficina_solicitante+'"';
            contador=1;
        }
        if (filtro_tipo_tarjeta!=""){
            if (filtro_oficina_solicitante!="")
                {queryString=queryString+",";}
            queryString=queryString+'"tipo_tarjeta":"'+filtro_tipo_tarjeta+'"';
        }
        if (filtro_formato_tarjeta!=""){
            if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="")
              {queryString=queryString+",";}
            queryString=queryString+'"formato_tarjeta":"'+filtro_formato_tarjeta+'"';
        }
        if (filtro_diseno_tarjeta!=""){
            if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="" ||filtro_formato_tarjeta!="")
              {queryString=queryString+",";}
            queryString=queryString+'"diseno_tarjeta":"'+filtro_diseno_tarjeta+'"';
        }
        if (filtro_estado!=""){
            if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="" ||filtro_formato_tarjeta!="")
              {queryString=queryString+",";}
            queryString=queryString+'"estado":"'+filtro_estado+'"';
        }
        if (filtro_fecha_inicio!="" && filtro_fecha_fin!=""){
          if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="" ||filtro_formato_tarjeta!=""||filtro_diseno_tarjeta!="" ||filtro_estado!="")
            {queryString=queryString+",";}
            queryString=queryString+'"fchproceso":{"$gte":{"$date":"'+ filtro_fecha_inicio+'"},"$lte":{"$date":"'+filtro_fecha_fin+'"}}';

        //q={"fechac":{"$gte":{"$date":"2019-12-04T00:00:50Z"},"$lte":{"$date":"2019-12-04T23:23:50Z"}}}
        }

          queryString=queryString+'}&';
          util_sisgein.printLog("queryString:"+queryString);

      //  var queryString='q={"idorden":'+idorden+'}&';
        var queryStrField='f={"_id":0}&';
        //var limFilter='l=1&';
        util_sisgein.printLog('ordenescalculadas?' + queryString +queryStrField +API_KEY_MLAB);

        var httpClient = requestJSON.createClient(URL_API_MLAB);
        httpClient.get('ordenescalculadas?' + queryString +queryStrField + API_KEY_MLAB,
        function(err, respuestaMLab, body) {
                 var response = {}
                 if(err) {
                   res.status(500);
                   response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo listado de ordenes calculadas."}
                 } else {
                    //util_sisgein.printLog('body: '+ JSON.stringify(body));
                          if(body.length > 0) {
                                  res.status(200);
                                  util_sisgein.printLog("Nùmero de ordenes: "+body.length);
                                  //response = body;
                                  response = {"codigo_mensaje":"01","mensaje" : "Listado exitoso'.","numero_elementos":+body.length,"ordenescalculadas":body}

                          } else {
                                  res.status(201);
                                  response = {"codigo_mensaje":"00","mensaje" : "Ningún elemento 'orden calculada'."}
                              }
                }
                res.send(response);
        });
      }
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":ex+""});
    }
};

exports.updateCalculatedOrder = function (req, res) {
    try{
        let idorden=req.body.idorden;
        let queryString='q={"idorden":'+idorden+'}&';
        let limFilter='l=1&';

        var httpClient = requestJSON.createClient(URL_API_MLAB);
        httpClient.get('ordenescalculadas?'+queryString+limFilter+API_KEY_MLAB,
        function(err, respuestaMLab, body) {
        util_sisgein.printLog('ordenescalculadas?'+queryString+limFilter+API_KEY_MLAB);
        if (!err){
              if (body.length>=1){
                    util_sisgein.printLog(body.length);
                    var fecha=new Date();
                    var fchauditoria=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                    var hrsauditoria=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
                  //Actualizo campos dla orden calculada

                    let updatedordencalculada = {
                      "idorden" : req.body.idorden,
                      "oficina_solicitante" : req.body.oficina_solicitante,
                      "tipo_tarjeta" : req.body.tipo_tarjeta,
                      "formato_tarjeta" : req.body.formato_tarjeta,
                      "diseno_tarjeta" : req.body.diseno_tarjeta,
                      "can_tarjetasord":req.body.can_tarjetasord,
                      "can_tarjetascon":req.body.can_tarjetasord,
                      "stock_actual":req.body.stock_actual,
                      "fchproceso":body[0].fchproceso,
                      "hrsproceso":body[0].hrsproceso,
                      "fchauditoria":fchauditoria,
                      "hrsauditoria":hrsauditoria,
                      "estado":req.body.estado
                    };
                    util_sisgein.printLog(updatedordencalculada);
                    util_sisgein.printLog(body[0]._id);
                    httpClient.put('ordenescalculadas/' + body[0]._id.$oid + '?' + API_KEY_MLAB, updatedordencalculada,
                    function(errPut, respuestaMLabPut, bodyPut){
                           var response = {};
                           if(errPut) {
                              res.status(400);
                              response = { "mensaje" : "Error actualizando ordencalculada."};
                           } else {
                                    res.status(200);
                                    util_sisgein.printLog('bodyPut'+ JSON.stringify(bodyPut));
                                    response =  {"codigo_mensaje":"01","mensaje":"Orden calculada actualizada satisfactoriamente","ordencalculada":bodyPut};
                           }
                           res.send(response);
                     });
              }else{
                res.status(400);
                res.send({"codigo_mensaje":"00","mensaje":"No se encontró la orden calculada con el id indicado"});
              }
            }else{
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al actualizar la orden calculada ","detalle":err.message});
          }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo ordencalculada.getAll","detalle":ex+""});
    }
};

exports.updateQuantityCalculatedOrder = function (req, res) {
    try{
        let idorden=req.body.idorden;
        let queryString='q={"idorden":'+idorden+'}&';
        let limFilter='l=1&';

        var httpClient = requestJSON.createClient(URL_API_MLAB);
        httpClient.get('ordenescalculadas?'+queryString+limFilter+API_KEY_MLAB,
        function(err, respuestaMLab, body) {
        util_sisgein.printLog('ordenescalculadas?'+queryString+limFilter+API_KEY_MLAB);
        if (!err){
            if (body.length>=1){
                util_sisgein.printLog(body.length);

                var fecha=new Date();
                var fchauditoria=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                var hrsauditoria=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
                //Actualizo campos dla orden calculada

                let updatedordencalculada = {
                  "idorden" : parseInt(req.body.idorden),
                  "oficina_solicitante" : body[0].oficina_solicitante,
                  "tipo_tarjeta" : body[0].tipo_tarjeta,
                  "formato_tarjeta" : body[0].formato_tarjeta,
                  "diseno_tarjeta" : body[0].diseno_tarjeta,
                  "can_tarjetasord":body[0].can_tarjetasord,
                  "can_tarjetascon":req.body.can_tarjetascon,
                  "stock_actual":body[0].stock_actual,
                  "fchproceso":body[0].fchproceso,
                  "hrsproceso":body[0].hrsproceso,
                  "fchauditoria":fchauditoria,
                  "hrsauditoria":hrsauditoria,
                  "estado":body[0].estado
                };
                util_sisgein.printLog(updatedordencalculada);
                util_sisgein.printLog(body[0]._id);
                httpClient.put('ordenescalculadas/' + body[0]._id.$oid + '?' + API_KEY_MLAB, updatedordencalculada,
                function(errPut, respuestaMLabPut, bodyPut){
                 var response = {};
                 if(errPut) {
                    res.status(400);
                    response = {
                       "mensaje" : "Error actualizando ordencalculada."
                     }
                 } else {
                          res.status(200);
                          util_sisgein.printLog('bodyPut'+ JSON.stringify(bodyPut));
                          response =  {"codigo_mensaje":"01","mensaje":"Orden calculada actualizada satisfactoriamente","ordencalculada":bodyPut}
                 }
                 res.send(response);
               });
            }else{
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"No se encontró la orden calculada con el id indicado"});
            }
          }else{
            res.status(400);
            res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al actualizar la orden calculada ","detalle":err.message});
          }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo ordencalculada.getAll","detalle":ex+""});
    }
};

exports.deleteCalculatedOrder = function (req, res) {
    try{
      var response={};
      var idorden = req.params.id;
      var queryString = 'q={"idorden":' + idorden + '}&';
      let limFilter='l=1&'
      util_sisgein.printLog('query delete:'+queryString);

      var httpClient = requestJSON.createClient(URL_API_MLAB);
      httpClient.get('ordenescalculadas?'+queryString+limFilter+API_KEY_MLAB,
      function(err, respuestaMLab, body) {
      if (err){
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al eliminar la orden calculada ","detalle":err.message});
      }else{
              util_sisgein.printLog("TAMAÑO"+body.length)
              if (body.length>=1){
                  util_sisgein.printLog(body[0]._id);

                  var httpClient = requestJSON.createClient(URL_API_MLAB);
                  httpClient.put('ordenescalculadas?' + queryString + API_KEY_MLAB,[{}],
                  function(errPut, resMLabPut, bodyPut){
                   util_sisgein.printLog(bodyPut);
                   if(errPut){
                          res.status(400);
                          response={"codigo_mensaje":"00","mensaje":"Ocurrio un error al eliminar la orden calculada ","detalle":errPut.message};
                   }else{
                          res.status(200);
                          response={"codigo_mensaje":"00","mensaje":"Orden calculada eliminada"};
                   }
                   res.send(response);
                  });
              }else{
                    res.status(400);
                    res.send({"codigo_mensaje":"00","mensaje":"No se encontró la orden calculada con el idorden indicado"});
              }
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo ordencalculada.getAll","detalle":ex+""});
    }
};

exports.desactivateCalculatedOrder = function (req, res) {
    try{
      var response={};
      var idorden = req.params.id;
      var queryString = 'q={"idorden":' + idorden + '}&';
      let limFilter='l=1&'
      util_sisgein.printLog('query delete:'+queryString);

      var httpClient = requestJSON.createClient(URL_API_MLAB);
      httpClient.get('ordenescalculadas?'+queryString+limFilter+API_KEY_MLAB,
      function(err, respuestaMLab, body) {
      if (err){
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al desactivar la orden calculada ","detalle":err.message});
      }else{
              util_sisgein.printLog("TAMAÑO"+body.length)
              if (body.length>=1){
                  var response=body[0];
                  let updatedordencalculada = {};
                  var fecha=new Date();
                  var fchauditoria=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                  var hrsauditoria=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();

                  Object.keys(response).forEach(key => updatedordencalculada[key] = response[key]);
                  updatedordencalculada["estado"]=0;
                  updatedordencalculada["fchauditoria"]=fchauditoria;
                  updatedordencalculada["hrsauditoria"]=hrsauditoria;

                  util_sisgein.printLog(updatedordencalculada);
                  util_sisgein.printLog(body[0]._id);

                  var httpClient = requestJSON.createClient(URL_API_MLAB);
                  httpClient.put('ordenescalculadas?' + queryString + API_KEY_MLAB,updatedordencalculada,
                  function(errPut, resMLabPut, bodyPut){
                       //var response = !errPut? bodyPut:{"codigo_mensaje":"00","mensaje":"ordencalculada Eliminado"};
                   util_sisgein.printLog(bodyPut);
                   if(errPut){
                          res.status(400);
                          response={"codigo_mensaje":"00","mensaje":"Ocurrio un error al desactivar la orden calculada ","detalle":errPut.message};
                   }else{
                          res.status(200);
                          response={"codigo_mensaje":"01","mensaje":"Orden calculada desactivada"};
                   }
                   res.send(response);
                  });
              }else{
                    res.status(400);
                    res.send({"codigo_mensaje":"00","mensaje":"No se encontró la orden calculada con el idorden indicado"});
              }
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo ordencalculada.getAll","detalle":ex+""});
    }
};

exports.confirmCalculatedOrder = function (req, res) {
    try{
      var response={};
      var idorden = req.params.id;
      var queryString = 'q={"idorden":' + idorden + '}&';
      let limFilter='l=1&'
      util_sisgein.printLog('query delete:'+queryString);

      var httpClient = requestJSON.createClient(URL_API_MLAB);
      httpClient.get('ordenescalculadas?'+queryString+limFilter+API_KEY_MLAB,
      function(err, respuestaMLab, body) {
      if (err){
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al activar la orden calculada ","detalle":err.message});
      }else{
              util_sisgein.printLog("TAMAÑO"+body.length)
              if (body.length>=1){
                  var response=body[0];
                  let updatedordencalculada = {};
                  var fecha=new Date();
                  var fchauditoria=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                  var hrsauditoria=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();

                  Object.keys(response).forEach(key => updatedordencalculada[key] = response[key]);
                  updatedordencalculada["estado"]=2;
                  updatedordencalculada["fchauditoria"]=fchauditoria;
                  updatedordencalculada["hrsauditoria"]=hrsauditoria;

                  util_sisgein.printLog(updatedordencalculada);
                  util_sisgein.printLog(body[0]._id);

                  var httpClient = requestJSON.createClient(URL_API_MLAB);
                  httpClient.put('ordenescalculadas?' + queryString + API_KEY_MLAB,updatedordencalculada,
                  function(errPut, resMLabPut, bodyPut){
                       //var response = !errPut? bodyPut:{"codigo_mensaje":"00","mensaje":"ordencalculada Eliminado"};
                   util_sisgein.printLog(bodyPut);
                   if(errPut){
                          res.status(400);
                          response={"codigo_mensaje":"00","mensaje":"Ocurrio un error al activar la orden calculada ","detalle":errPut.message};
                   }else{
                          res.status(200);
                          response={"codigo_mensaje":"00","mensaje":"ordencalculada activado"};
                   }
                   res.send(response);
                  });
              }else{
                    res.status(400);
                    res.send({"codigo_mensaje":"00","mensaje":"No se encontró la orden calculada con el id indicado"});
              }
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo ordencalculada.getAll","detalle":ex+""});
    }
};

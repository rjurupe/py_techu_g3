var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;
const moment = require('node-moment');

require('dotenv').config();

exports.getAllOffice = function(req,res) {
try{

   var queryString = 'f={"_id":0}&';
   var sortString='s={"DESCRIPCION_OFICINA":1}&';
   console.log("[getAllOffice] " + queryString + API_KEY_MLAB);
   var httpClient = requestJSON.createClient(URL_API_MLAB);
   httpClient.get('oficinas?' + queryString + sortString+API_KEY_MLAB,
   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo las oficinas."}
           res.status(500);
       } else {
         if(body.length > 0) {
           res.status(200);
           console.log(body.length);
           response = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":body.length,"oficinas":body}
         } else {
           response = {"codigo_mensaje":"00","mensaje" : "No existe oficinas a listar"}
           res.status(201);
         }
       }
       res.send(response);
     });
   }catch(ex){
       res.status(500);
       res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo order getAllOffice","detalle":ex+""});
   }
};

exports.getByIdOffice = function(req,res) {
try{
      util_sisgein.printLog("iniciando metodo getByIdOffice");
      var codigo_oficina = req.params.id;
      console.log("[codigo oficina]  " + codigo_oficina);

      var queryString = 'q={"CODIGO_OFICINA":"'  + codigo_oficina + '"}&'; //int32
      console.log("queryString => " + queryString);
      var queryStrField = 'f={"_id":0}&';

      var httpClient = requestJSON.createClient(URL_API_MLAB);
      httpClient.get('oficinas?' + queryString + queryStrField + API_KEY_MLAB,
      function(err, respuestaMLab, body) {
          var response = {};
          if(err) {
              response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo datos de la oficina solicitada."}
              res.status(500);
          } else {
            if(body.length > 0) {
              res.status(200);
              console.log(body.length);
              response = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":body.length,"oficinas":body}
            } else {
              response = {"codigo_mensaje":"00","mensaje" : "No existe la oficina  solicitada"}
              res.status(201);
            }
          }
          res.send(response);
      });
}catch(ex){
    res.status(500);
    res.send({"codigo_mensaje":"00","mensaje":"Excepcion en el metodo getByIdOffice","detalle":ex+""});
}
};

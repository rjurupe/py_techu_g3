var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;

require('dotenv').config();

exports.getAllUsers = function (req, res) {
try{
    var perfiles={};
    var oficinas={};
    //Oficinas
    util_sisgein.printLog("Obtener todas las oficinas");
    var queryStringoficina = 'f={"_id":0}&';
    var httpClientOficina = requestJSON.createClient(URL_API_MLAB);
    httpClientOficina.get('oficinas?' + queryStringoficina + API_KEY_MLAB,
    function(erroficina, respuestaMLaboficina, bodyoficina) {
              var responseoficinas = {};
              if(erroficina) {
                  responseoficinas = {"codigo_mensaje":"00","mensaje" : "Error obteniendo las oficinas."}
                  res.status(500);
                  res.send(responseoficinas);
              } else {
                      if(bodyoficina.length > 0) {
                        util_sisgein.printLog("Nro de oficinas: "+bodyoficina.length);
                        oficinas=bodyoficina;
// Perfiles
    util_sisgein.printLog("Obtener todos los perfiles");
    var queryStringperfil = 'f={"_id":0}&';
    var httpClientperfil = requestJSON.createClient(URL_API_MLAB);
    httpClientperfil.get('perfiles?' + queryStringperfil + API_KEY_MLAB,
    function(errperfil, respuestaMLabperfil, bodyperfil) {
                 var responseperfiles = {};
                 if(errperfil){
                         res.status(400);
                         responseperfiles = {"mensaje" : "Error obteniendo perfiles.","detalle":err.message};
                         res.send(responseperfiles);
                 }else  {
                         if(bodyperfil.length > 0) {
                              util_sisgein.printLog("Nro de perfiles: "+bodyperfil.length);
                              perfiles = bodyperfil;

//usuarios
util_sisgein.printLog("Obtener todos los usuarios: getAll");
var httpClient = requestJSON.createClient(URL_API_MLAB);
var queryString = 'f={"_id":0}&';
httpClient.get('usuarios?' + queryString + API_KEY_MLAB,
function(err, respuestaMLab, body) {
         var response = {};
         if(err){
                 res.status(400);
                 response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo usuarios.","detalle":err.message};
         }else  {
                 if(body.length > 0) {
                      res.status(200);
                      util_sisgein.printLog("Nro de usuarios: "+body.length);
                      let usuarios=[];
                      let usuario={};

                      for (x=0;x<body.length;x++){
                        usuario = {};
                        Object.keys(body[x]).forEach(key => usuario[key] = body[x][key]);
                        var p=0;
                        var o=0;
                        var encuentraperfil=false;
                        var encuentraoficina=false;
                        var descripcionperfil="";
                        var descripcionoficina="";
                        util_sisgein.printLog("usuario: "+ usuario["idperfil"]+"indice:"+x);
                        if (typeof usuario["idperfil"] !== 'undefined') {
                        if (perfiles.length>=1){
                            while(!encuentraperfil && p<perfiles.length){
                                  if (usuario["idperfil"]==perfiles[p]["idperfil"])
                                    {
                                      encuentraperfil=true;
                                      descripcionperfil=perfiles[p]["descripcion"];
                                    }else {
                                      p++;
                                    }

                            }
                        }
                        }

                        if (typeof usuario["oficina"] !== 'undefined') {
                        if (oficinas.length>=1){
                            while((!encuentraoficina) && o<oficinas.length){
                                  if (typeof oficinas[o]["CODIGO_OFICINA"] !== 'undefined'){
                                      if (usuario["oficina"]==oficinas[o]["CODIGO_OFICINA"]){
                                          encuentraoficina=true;
                                          descripcionoficina=oficinas[o]["DESCRIPCION_OFICINA"];
                                      }else {
                                        o++;
                                        encuentra=false;
                                        descripcionoficina="";
                                      }
                                  }else {
                                    o++;
                                    encuentra=false;
                                    descripcionoficina="";
                                  }
                            }
                        }
                        }
                        usuario["descripcionperfil"]=descripcionperfil;
                        usuario["descripcionoficina"]=descripcionoficina;
                        usuarios[x]=usuario;
                      }
                        response = {"codigo_mensaje":"01","mensaje" : "La lista de usuarios se cargò satisfactoriamente.","numero_elementos":body.length,"usuarios":usuarios};
                 }else{
                      res.status(201);
                      response = {"codigo_mensaje":"00","mensaje" : "La lista de usuarios esta vacía."}
                 }
         }
        res.send(response);
});

//Usuarios
                   }else{
                        res.status(404);
                        responseperfiles = {"mensaje" : "La lista de perfiles esta vacía."}
                        res.send(responseperfiles);
                   }
           }

  });

//Perfiles

                  } else {
                    responseoficinas = {"codigo_mensaje":"00","mensaje" : "No existe oficinas a listar"}
                    res.status(404);
                    res.send(responseoficinas);
                  }
          }

  });
}catch(ex){
    res.status(500);
    res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.getAll","detalle":ex+""});
}
};


exports.insertUser = function (req, res) {
    var registro=req.body.registro;
    var password = req.body.password;
    var hashPassword = bcrypt.hashSync(password, parseInt(SALT_ROUNDS));
    try{


      if (typeof req.body.registro=='undefined' ||
          typeof req.body.apellidos=='undefined'  ||
          typeof req.body.nombres=='undefined'  ||
          typeof req.body.idperfil=='undefined' ||
          typeof req.body.oficina=='undefined'  ||
          typeof req.body.direccion=='undefined'  ||
          typeof req.body.rutafoto=='undefined' ||
          typeof req.body.password=='undefined' ||
          typeof req.body.estado=='undefined'){
            res.status(401);
            response = {"codigo_mensaje":"00","mensaje" : "Faltan ingresar datos obligatorios."};
            res.send(response);
      }
         util_sisgein.printLog("Crear Usuario");
         util_sisgein.printLog(req.body.registro);
         util_sisgein.printLog(req.body.password);
         util_sisgein.printLog(hashPassword);

         var queryString='q={"registro":"'+registro+'"}&';

         var httpClient=requestJSON.createClient(URL_API_MLAB);
         util_sisgein.printLog('query de userCreate:'+'usuarios?'+queryString+API_KEY_MLAB);
         httpClient.get('usuarios?'+queryString+API_KEY_MLAB,
         function(err,respuestaMLab,body){
              if (err) {
                    res.status(400);
                    res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al buscar el usuario","detalle":err.message});
              }else{
                    let response = body[0];
                    util_sisgein.printLog("usuario creado: "+response);
                    if(typeof response!='undefined'){
                          res.status(400);
                          res.send({"codigo_mensaje":"00","mensaje":"El usuario ya se encuentra registrado."});
                    }else{
                          httpClient.get('usuarios?'+ API_KEY_MLAB,
                          function(err, respuestaMLab, body) {
                              if(err) {
                                    res.status(400);
                                    res.send({"codigo_mensaje":"00","mensaje":"Ocurrio un error al registrar el usuario ","detalle":err.message});
                              }else{
                                    res.status(200);
                                    var newId=body.length +1;
                                    var fecha=new Date();
                                    var fchcreacion=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                                    var hrscreacion=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
                                    util_sisgein.printLog("new Id:"+newId);
                                    util_sisgein.printLog("fecha:"+fecha);
                                    util_sisgein.printLog("fchcreacion:"+fchcreacion);
                                    util_sisgein.printLog("hrscreacion:"+hrscreacion);
                                    var newUser = {
                                      'registro' : req.body.registro,
                                      'oficina' : req.body.oficina,
                                      'apellidos': req.body.apellidos,
                                      'nombres': req.body.nombres,
                                      'direccion':req.body.direccion,
                                      'idperfil' : req.body.idperfil,
                                      'rutafoto':req.body.rutafoto,
                                      'estado' : req.body.estado,
                                      'password' : hashPassword,
                                      'fchcreacion' : fchcreacion,
                                      'hrscreacion' : hrscreacion,
                                      'fchauditoria': fchcreacion,
                                      'hrsauditoria': hrscreacion
                                    };
                                    httpClient.post(URL_API_MLAB+'usuarios?'+API_KEY_MLAB,newUser,
                                    function (err,respuestaMLab,body) {
                                        if (err) {
                                            res.status(400);
                                            res.send({"codigo_mensaje":"00","mensaje":"Ocurrio un error al registrar el usuario ","detalle":err.message});
                                        }else{
                                              res.status(200);
                                              if(body!=null){
                                                 res.send({"codigo_mensaje":"01","mensaje":"Usuario agregado satisfactoriamente","usuario":newUser});
                                              }else{
                                                  res.status(400);
                                                 res.send({"codigo_mensaje":"00","mensaje":"Ocurrio un error al registrar el usuario"});
                                              }
                                        }
                                    });
                              }
                          });
                    }
              }
         });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.getAll","detalle":ex+""});
    }
};

//Obtener usuario por Id
exports.getByIdUser = function (req, res) {
    try{
        util_sisgein.printLog("Obtener usuario por Id:"+ req.params.id);
        var registro=req.params.id;
        var queryString='q={"registro":"'+registro+'"}&';
        var queryStrField='f={"_id":0}&';
        var limFilter='l=1&';
        util_sisgein.printLog('usuarios?' + queryString +queryStrField +limFilter+ API_KEY_MLAB);
        var httpClient = requestJSON.createClient(URL_API_MLAB);
        httpClient.get('usuarios?' + queryString +queryStrField +limFilter+ API_KEY_MLAB,
        function(err, respuestaMLab, body) {
                 var response = {};
                 if(err) {
                   response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo usuario."}
                   res.status(500);
                 } else {
                          if(body.length > 0) {
                              util_sisgein.printLog(body.length);
                              response = {"codigo_mensaje":"01","mensaje" : "La lista de usuarios se cargò satisfactoriamente.","numero_elementos":body.length,"usuarios":body};
                          } else {
                              response = {"codigo_mensaje":"00","mensaje" : "Ningún elemento 'usuario'."}
                              res.status(404);
                          }
                }
                res.send(response);
        });
    }catch(ex){
        res.status(500);
         res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.getById","detalle":ex+""});
    }
};

exports.updateUser = function (req, res) {
    try{
        if (typeof req.body.flagcambioclave=='undefined' ||
            typeof req.body.registro=='undefined' ||
            typeof req.body.apellidos=='undefined'  ||
            typeof req.body.nombres=='undefined'  ||
            typeof req.body.idperfil=='undefined' ||
            typeof req.body.oficina=='undefined'  ||
            typeof req.body.direccion=='undefined'  ||
            typeof req.body.rutafoto=='undefined' ||
            typeof req.body.password=='undefined' ||
            typeof req.body.estado=='undefined'){
              res.status(401);
              var response = {"codigo_mensaje":"00","mensaje" : "Faltan ingresar datos obligatorios."};
              res.send(response);
        }
        let flagcambioclave=req.body.flagcambioclave;
        let password = req.body.password;
        var hashPassword = bcrypt.hashSync(password, parseInt(SALT_ROUNDS));

        if (!flagcambioclave){
             hashPassword = password;
        }

        let registro=req.body.registro;
        let queryString='q={"registro":"'+registro+'"}&';
        let limFilter='l=1&';

        var httpClient = requestJSON.createClient(URL_API_MLAB);
        httpClient.get('usuarios?'+queryString+limFilter+API_KEY_MLAB,
        function(err, respuestaMLab, body) {
        util_sisgein.printLog('usuarios?'+queryString+limFilter+API_KEY_MLAB);
        if (!err){
            if (body.length>=1){
                util_sisgein.printLog(body.length);

                var fecha=new Date();
                var fchauditoria=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                var hrsauditoria=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
                //Actualizo campos del usuario

                let updatedUser = {
                  "registro" : req.body.registro,
                  "apellidos" : req.body.apellidos,
                  "nombres" : req.body.nombres,
                  "idperfil" : req.body.idperfil,
                  "oficina" : req.body.oficina,
                  "direccion":req.body.direccion,
                  "rutafoto":req.body.rutafoto,
                  "password" : hashPassword,
                  "fchcreacion":body[0].fchcreacion,
                  "hrscreacion":body[0].hrscreacion,
                  "fchauditoria":fchauditoria,
                  "hrsauditoria":hrsauditoria,
                  "estado":req.body.estado
                };
                util_sisgein.printLog(updatedUser);
                util_sisgein.printLog(body[0]._id);
                httpClient.put('usuarios/' + body[0]._id.$oid + '?' + API_KEY_MLAB, updatedUser,
                function(errPut, respuestaMLabPut, bodyPut){
                 var response = {};
                 if(errPut) {
                    res.status(400);
                    response = {"codigo_mensaje":"00",
                       "mensaje" : "Error actualizando usuario.","detalle":errPut.message
                     }
                 } else {
                          res.status(200);
                          util_sisgein.printLog('bodyPut'+ JSON.stringify(bodyPut));
                          response =  {"codigo_mensaje":"01","mensaje":"Usuario actualizado satisfactoriamente","usuario":bodyPut}
                 }
                 res.send(response);
               });
            }else{
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"No se encontró el usuario con el id indicado"});
            }
          }else{
            res.status(400);
            res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al actualizar el usuario ","detalle":err.message});
          }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.update","detalle":ex+""});
    }
};

exports.deleteUser = function (req, res) {
    try{
      var response={};
      var registro = req.params.id;
      var queryString = 'q={"registro":"' + registro + '"}&';
      let limFilter='l=1&'
      util_sisgein.printLog('query delete:'+queryString);

      var httpClient = requestJSON.createClient(URL_API_MLAB);
      httpClient.get('usuarios?'+queryString+limFilter+API_KEY_MLAB,
      function(err, respuestaMLab, body) {
      if (err){
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al eliminar el usuario ","detalle":err.message});
      }else{
              util_sisgein.printLog("TAMAÑO"+body.length)
              if (body.length>=1){
                  util_sisgein.printLog(body[0]._id);

                  var httpClient = requestJSON.createClient(URL_API_MLAB);
                  httpClient.put('usuarios?' + queryString + API_KEY_MLAB,[{}],
                  function(errPut, resMLabPut, bodyPut){
                   util_sisgein.printLog(bodyPut);
                   if(errPut){
                          res.status(400);
                          response={"codigo_mensaje":"00","mensaje":"Ocurrio un error al eliminar el usuario ","detalle":errPut.message};
                   }else{
                          res.status(200);
                          response={"codigo_mensaje":"01","mensaje":"Usuario eliminado"};
                   }
                   res.send(response);
                  });
              }else{
                    res.status(400);
                    res.send({"codigo_mensaje":"00","mensaje":"No se encontró el usuario con el registro indicado"});
              }
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.delete","detalle":ex+""});
    }
};

exports.desactivateUser = function (req, res) {
    try{
      var response={};
      var registro = req.params.id;
      var queryString = 'q={"registro":"' + registro + '"}&';
      let limFilter='l=1&'
      util_sisgein.printLog('query delete:'+queryString);

      var httpClient = requestJSON.createClient(URL_API_MLAB);
      httpClient.get('usuarios?'+queryString+limFilter+API_KEY_MLAB,
      function(err, respuestaMLab, body) {
      if (err){
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al desactivar el usuario ","detalle":err.message});
      }else{
              util_sisgein.printLog("TAMAÑO"+body.length)
              if (body.length>=1){
                  var response=body[0];
                  let updatedUser = {};
                  var fecha=new Date();
                  var fchauditoria=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                  var hrsauditoria=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();

                  Object.keys(response).forEach(key => updatedUser[key] = response[key]);
                  updatedUser["estado"]=0;
                  updatedUser["fchauditoria"]=fchauditoria;
                  updatedUser["hrsauditoria"]=hrsauditoria;

                  util_sisgein.printLog(updatedUser);
                  util_sisgein.printLog(body[0]._id);

                  var httpClient = requestJSON.createClient(URL_API_MLAB);
                  httpClient.put('usuarios?' + queryString + API_KEY_MLAB,updatedUser,
                  function(errPut, resMLabPut, bodyPut){
                       //var response = !errPut? bodyPut:{"codigo_mensaje":"00","mensaje":"Usuario Eliminado"};
                   util_sisgein.printLog(bodyPut);
                   if(errPut){
                          res.status(400);
                          response={"codigo_mensaje":"00","mensaje":"Ocurrio un error al desactivar el usuario ","detalle":errPut.message};
                   }else{
                          res.status(200);
                          response={"codigo_mensaje":"01","mensaje":"Usuario desactivado"};
                   }
                   res.send(response);
                  });
              }else{
                    res.status(400);
                    res.send({"codigo_mensaje":"00","mensaje":"No se encontró el usuario con el registro indicado"});
              }
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.desactivate","detalle":ex+""});
    }
};

exports.activateUser = function (req, res) {
    try{
      var response={};
      var registro = req.params.id;
      var queryString = 'q={"registro":"' + registro + '"}&';
      let limFilter='l=1&'
      util_sisgein.printLog('query delete:'+queryString);

      var httpClient = requestJSON.createClient(URL_API_MLAB);
      httpClient.get('usuarios?'+queryString+limFilter+API_KEY_MLAB,
      function(err, respuestaMLab, body) {
      if (err){
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al activar el usuario ","detalle":err.message});
      }else{
              util_sisgein.printLog("TAMAÑO"+body.length)
              if (body.length>=1){
                  var response=body[0];
                  let updatedUser = {};
                  var fecha=new Date();
                  var fchauditoria=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                  var hrsauditoria=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();

                  Object.keys(response).forEach(key => updatedUser[key] = response[key]);
                  updatedUser["estado"]=1;
                  updatedUser["fchauditoria"]=fchauditoria;
                  updatedUser["hrsauditoria"]=hrsauditoria;

                  util_sisgein.printLog(updatedUser);
                  util_sisgein.printLog(body[0]._id);

                  var httpClient = requestJSON.createClient(URL_API_MLAB);
                  httpClient.put('usuarios?' + queryString + API_KEY_MLAB,updatedUser,
                  function(errPut, resMLabPut, bodyPut){
                       //var response = !errPut? bodyPut:{"codigo_mensaje":"00","mensaje":"Usuario Eliminado"};
                   util_sisgein.printLog(bodyPut);
                   if(errPut){
                          res.status(400);
                          response={"codigo_mensaje":"00","mensaje":"Ocurrio un error al activar el usuario ","detalle":errPut.message};
                   }else{
                          res.status(200);
                          response={"codigo_mensaje":"01","mensaje":"Usuario activado"};
                   }
                   res.send(response);
                  });
              }else{
                    res.status(400);
                    res.send({"codigo_mensaje":"00","mensaje":"No se encontró el usuario con el id indicado"});
              }
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.activate","detalle":ex+""});
    }
};

exports.login = function (req, res){
try{
      util_sisgein.printLog("iniciando el metodo login");
      var registro=req.body.registro;
      var pass=req.body.password;
      var queryString ='q={"registro":"'+registro+'"}&';
      var limFilter='l=1&';
      var httpClient=requestJSON.createClient(URL_API_MLAB);
      httpClient.get('usuarios?'+queryString+limFilter+API_KEY_MLAB,
      function(err,respuestaMLab,body){
      if(err){
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al obtener  el usuario","detalle":err.message});
      }else{
              util_sisgein.printLog("resultado: "+body.length);
              if(body.length>=1){
                    if (typeof body[0].logged=='undefined'){
                          if (bcrypt.compareSync( pass , body[0].password)) {
                               var tokenData = {
                                                   username: registro
                                               };
                               var token = jwt.sign(tokenData,JWT_SECRET_PASS, {
                                expiresIn: 60 * 60 * 1 // expires in 1 hours
                               });
                               util_sisgein.printLog("body[0].password:  "+body[0].password);
                               util_sisgein.printLog("pass:  "+pass);

                               let logged='{"$set":{"logged":true,"token":"'+token+'"}}';
                               registro=body[0].registro;
                               util_sisgein.printLog(logged);

                               httpClient.put('usuarios?'+queryString+API_KEY_MLAB,JSON.parse(logged),
                               function(errPut,respuestaMLabPut,bodyPut){
                               if(errPut){
                                         res.status(400);
                                         res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al loguearse el usuario","detalle":err.message});
                               }else{
                                         util_sisgein.printLog("bodyput: "+bodyPut);
                                         if(bodyPut.n==1){
                                              res.status(200);
                                              res.send({"codigo_mensaje":"01","mensaje":"Login Correcto","usuario":body[0],"token":token});
                                         }else{
                                              res.status(400);
                                              res.send({"codigo_mensaje":"00","mensaje":"No se pudo loguear el usuario"});
                                         }
                               }
                               });
                          }else{
                                   res.status(400);
                                   util_sisgein.printLog('Password doesn\'t match');
                                   res.send({"codigo_mensaje":"00","mensaje":"Login Incorrecto. Los datos son inválidos"});
                               }
                    }else{
                          res.status(210);
                          util_sisgein.printLog('El usuario ya se encuentra logueado.');
                          res.send({"codigo_mensaje":"00","mensaje":"El usuario ya se encuentra logueado"});
                    }
              }else{
                    res.status(400);
                    util_sisgein.printLog('usuario no registrado');
                    res.send({"codigo_mensaje":"00","mensaje":"usuario no registrado"});
              }
      }
      });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.login","detalle":ex+""});
    }
};


exports.logout = function (req, res){
    try{
            let registro=req.body.registro;
            let queryString ='q={"registro":"'+registro+'"}&';
            let limFilter='l=1&';

            var httpClient=requestJSON.createClient(URL_API_MLAB);
            httpClient.get('usuarios?'+queryString+limFilter+API_KEY_MLAB,
            function(err,respuestaMLab,body){
                if(err){
                      res.status(400);
                      res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al obtener el usuario","detalle":err.message});
                }else{
                      util_sisgein.printLog("leght"+body.length);
                      if(body.length>=1){
                            let logged='{"$unset":{"logged":"","token":""}}';
                            util_sisgein.printLog(logged);

                            httpClient.put('usuarios?'+queryString+API_KEY_MLAB,JSON.parse(logged),
                            function(errPut,respuestaMLabPut,bodyPut){
                                      if(errPut){
                                        res.status(400);
                                        res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al desloguearse el usuario","detalle":
                                        err.message});
                                      }
                                      else{
                                            if(bodyPut.n==1){
                                                  res.status(200);
                                                  res.send({"codigo_mensaje":"01","mensaje":"Logout Correcto"});

                                            }else{
                                                  res.status(400);
                                                  res.send({"codigo_mensaje":"00","mensaje":"No se puedo desloguear."});
                                            }
                                      }
                            });
                      }else{
                            res.status(400);
                            util_sisgein.printLog('usuario no registrado');
                            res.send({"codigo_mensaje":"00","mensaje":"usuario no registrado"});
                      }
                }
            });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo usuario.logout","detalle":ex+""});
    }
};

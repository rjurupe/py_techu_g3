var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var requestJSON=require('request-json');

const API_KEY_MLAB = "apiKey="+process.env.API_KEY_MLAB;
const URL_API_MLAB=process.env.URL_API_MLAB;
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;
const moment = require('node-moment');

require('dotenv').config();

exports.getAllOrders = function(req,res) {
  try{

   var queryString = 'f={"_id":0}&';
   console.log("[getOrders] " + queryString + API_KEY_MLAB);
   var httpClient = requestJSON.createClient(URL_API_MLAB);
   httpClient.get('ordenes?' + queryString + API_KEY_MLAB,
   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo las ordenes."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":body.length,"orden":body}
         } else {
           response = {"codigo_mensaje":"00","mensaje" : "No existe ordenes a procesar"}
           res.status(404);
         }
       }
       res.send(response);
     });
   }catch(ex){
       res.status(500);
       res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo order.getAllOrder","detalle":ex+""});
   }
};

exports.getOrder = function(req,res) {
  var nro_orden = req.params.id;
  console.log("[getOrder] nro Orden " + nro_orden);

  //var queryString = 'q={"idorden":"'  + nro_orden + '"}&'; //string
  var queryString = 'q={"nro_orden":'  + nro_orden + '}&'; //int32
  console.log("queryString => " + queryString);
  var queryStrField = 'f={"_id":0}&';

  var httpClient = requestJSON.createClient(URL_API_MLAB);
  httpClient.get('ordenes?' + queryString + queryStrField + API_KEY_MLAB,

  function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo datos de la Orden Solicitada."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":body.length,"orden":body}
        } else {
          response = {"codigo_mensaje":"00","mensaje" : "No existe la Orden solicitada"}
          res.status(404);
        }
      }
      res.send(response);
    });
}

exports.getByFilterOrder = function (req, res) {
try{

    var tipos_tarjeta={};
    var formatos_tarjeta={};
    var disenos_tarjeta={};
    var oficinas={};

//Oficinas
    util_sisgein.printLog("Obtener todas las oficinas");
    var queryStringoficina = 'f={"_id":0}&';
    var httpClientOficina = requestJSON.createClient(URL_API_MLAB);
    httpClientOficina.get('oficinas?' + queryStringoficina + API_KEY_MLAB,
    function(erroficina, respuestaMLaboficina, bodyoficina) {
              var responseoficinas = {};
              if(erroficina) {
                  responseoficinas = {"codigo_mensaje":"00","mensaje" : "Error obteniendo las oficinas."}
                  res.status(500);
                  res.send(responseoficinas);
              } else {
                      if(bodyoficina.length > 0) {
                        util_sisgein.printLog("Nro de oficinas: "+bodyoficina.length);
                        oficinas=bodyoficina;
//TIPO tarjeta

     util_sisgein.printLog('Iniciando metodo getAllCardType ');
      var queryStringTipo = 'f={"_id":0}&';
      util_sisgein.printLog("[getAllCardType] " + queryStringTipo + API_KEY_MLAB);
      var httpClient = requestJSON.createClient(URL_API_MLAB);
      httpClient.get('tipos_tarjeta?' + queryStringTipo + API_KEY_MLAB,
      function(errtipo, respuestaMLabtipo, bodytipo) {
          var responsetipo = {};
          if(errtipo) {
              responsetipo = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los tipos de tarjeta."}
              res.status(500);
          } else {
            if(bodytipo.length > 0) {
             util_sisgein.printLog("Nro de tipo de tarjeta: "+bodytipo.length);
              tipos_tarjeta=bodytipo;

//Formato Tarjeta
       util_sisgein.printLog('Iniciando metodo getAllCardFormat ');
       var queryStringFormato = 'f={"_id":0}&';
       util_sisgein.printLog("[getAllCardFormat] " + queryStringFormato + API_KEY_MLAB);
       var httpClient = requestJSON.createClient(URL_API_MLAB);
       httpClient.get('formatos_tarjeta?' + queryStringFormato + API_KEY_MLAB,
       function(errformato, respuestaMLabformato, bodyformato) {
          var responseformato = {};
          if(errformato) {
              responseformato = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los formatos de tarjeta."}
              res.status(500);
          } else {
            if(bodyformato.length > 0) {
              util_sisgein.printLog("Nro de formatos de tarjeta: "+bodyformato.length);
              formatos_tarjeta=bodyformato;
// Diseno Tarjeta
        util_sisgein.printLog('Iniciando metodo getAllCardDesign ');
        var queryStringDiseno = 'f={"_id":0}&';
        util_sisgein.printLog("[getAllCardDesign] " + queryStringDiseno + API_KEY_MLAB);
        var httpClientDiseno = requestJSON.createClient(URL_API_MLAB);
        httpClientDiseno.get('disenos_tarjeta?' + queryStringDiseno + API_KEY_MLAB,
        function(errdiseno, respuestaMLabdiseno, bodydiseno) {
          util_sisgein.printLog('marca diseno ');
            var responsediseno = {};
            if(errdiseno) {
                responsediseno = {"codigo_mensaje":"00","mensaje" : "Error obteniendo los s de tarjeta."}
                res.status(500);
            } else {
              if(bodydiseno.length > 0) {
                console.log(bodydiseno.length);
                disenos_tarjeta=bodydiseno;
//Ordenes
        util_sisgein.printLog("Iniciando Obtener Ordenes por filtros");

        var filtro_oficina_solicitante=req.body.filtro_oficina_solicitante;
        var filtro_tipo_tarjeta=req.body.filtro_tipo_tarjeta;
        var filtro_formato_tarjeta=req.body.filtro_formato_tarjeta;
        var filtro_diseno_tarjeta=req.body.filtro_diseno_tarjeta;
        var filtro_estado=req.body.filtro_estado;
        var filtro_fecha_inicio=req.body.filtro_fecha_inicio;
        var filtro_fecha_fin=req.body.filtro_fecha_fin;

        var queryString='q={';
        var contador=0;

      if (filtro_oficina_solicitante=="" && filtro_tipo_tarjeta==""
          && filtro_formato_tarjeta=="" && filtro_diseno_tarjeta=="" && filtro_fecha_inicio==""
          && filtro_fecha_fin=="" &&  filtro_estado==""){

            var queryString = 'f={"_id":0}&';
            var httpClient = requestJSON.createClient(URL_API_MLAB);
            httpClient.get('ordenes?' + queryString + API_KEY_MLAB,
            function(err, respuestaMLab, body) {
                var response = {};
                if(err) {
                    response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo las ordenes."}
                    res.status(500);
                } else {
                  if(body.length > 0) {
                    res.status(200);
                          console.log(body.length);
                          response = {"codigo_mensaje":"01","mensaje" : "La lista ha sido cargada satisfactoriamente.","numero_elementos":body.length,"ordenes":body}
                  } else {
                          response = {"codigo_mensaje":"00","mensaje" : "No existe ordenes a consultar"}
                          res.status(201);
                  }
                }
                res.send(response);
              });
          }
      else{
        if (filtro_oficina_solicitante!=""){
            queryString=queryString+'"oficina_solicitante":"'+filtro_oficina_solicitante+'"';
            contador=1;
        }
        if (filtro_tipo_tarjeta!=""){
            if (filtro_oficina_solicitante!="")
                {queryString=queryString+",";}
            queryString=queryString+'"tipo_tarjeta":"'+filtro_tipo_tarjeta+'"';
        }
        if (filtro_formato_tarjeta!=""){
            if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="")
              {queryString=queryString+",";}
            queryString=queryString+'"formato_tarjeta":"'+filtro_formato_tarjeta+'"';
        }
        if (filtro_diseno_tarjeta!=""){
            if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="" ||filtro_formato_tarjeta!="")
              {queryString=queryString+",";}
            queryString=queryString+'"diseno_tarjeta":"'+filtro_diseno_tarjeta+'"';
        }
        if (filtro_estado!=""){
            if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="" ||filtro_formato_tarjeta!="")
              {queryString=queryString+",";}
            queryString=queryString+'"situacion_orden":"'+filtro_estado+'"';
        }
        if (filtro_fecha_inicio!="" && filtro_fecha_fin!=""){
          if (filtro_oficina_solicitante!=""||filtro_tipo_tarjeta!="" ||filtro_formato_tarjeta!=""||filtro_diseno_tarjeta!="" ||filtro_estado!="")
            {queryString=queryString+",";}

            queryString=queryString+'"fecha_alta":{"$gte":{"$date":"'+ filtro_fecha_inicio+'"},"$lte":{"$date":"'+filtro_fecha_fin+'"}}';

        //q={"fechac":{"$gte":{"$date":"2019-12-04T00:00:50Z"},"$lte":{"$date":"2019-12-04T23:23:50Z"}}}
        }

          queryString=queryString+'}&';
          util_sisgein.printLog("queryString:"+queryString);

      //  var queryString='q={"idorden":'+idorden+'}&';
        var queryStrField='f={"_id":0}&';
        //var limFilter='l=1&';
        util_sisgein.printLog('ordenes?' + queryString +queryStrField +API_KEY_MLAB);

        var httpClient = requestJSON.createClient(URL_API_MLAB);
        httpClient.get('ordenes?' + queryString +queryStrField + API_KEY_MLAB,
        function(err, respuestaMLab, body) {
                 var response = {}
                 if(err) {
                   res.status(500);
                   response = {"codigo_mensaje":"00","mensaje" : "Error obteniendo listado de ordenes."}
                 } else {
                          if(body.length > 0) {


                            res.status(200);
                            console.log(body.length);
                            var arrayordenes=[];
                            var orden={};
                            arrayordenes=body;

                            for (x=0;x<body.length;x++){
                              orden = {};
                              Object.keys(body[x]).forEach(key => orden[key] = body[x][key]);
                              var t=0;
                              var f=0;
                              var d=0;
                              var o=0;
                              var encuentratipotarjeta=false;
                              var encuentraformatotarjeta=false;
                              var encuentradisenotarjeta=false;
                              var encuentraoficina=false;
                              var descripciontipotarjeta="";
                              var descripcionformatotarjeta="";
                              var descripciondisenotarjeta="";
                              var descripcionoficina="";
                              if (typeof orden["oficina_solicitante"] !== 'undefined') {
                              if (oficinas.length>=1){
                                  while(o<oficinas.length && (!encuentraoficina)){
                                    var oficina =oficinas[o];
                                        if (typeof oficina["CODIGO_OFICINA"] !== 'undefined'){
                                              if (orden["oficina_solicitante"]==oficinas[o]["CODIGO_OFICINA"]){
                                                  encuentraoficina=true;
                                                  descripcionoficina=oficinas[o]["DESCRIPCION_OFICINA"];
                                              }else {
                                                o++;
                                              }
                                        }else {
                                          o++;
                                        }
                                  }
                              }
                              }

                              util_sisgein.printLog("orden  tipo tarjeta: "+ orden["tipo_tarjeta"]+" indice:"+x);
                              if (typeof orden["tipo_tarjeta"] !== 'undefined') {
                              if (tipos_tarjeta.length>=1){
                                  while((!encuentratipotarjeta)&& (t<tipos_tarjeta.length)){
                                        if (orden["tipo_tarjeta"]==tipos_tarjeta[t]["tipo_tarjeta"])
                                          {
                                            encuentratipotarjeta=true;
                                            descripciontipotarjeta=tipos_tarjeta[t]["desc_tipo"];
                                          }else {
                                            t++;
                                          }
                                  }
                              }
                              }

                              if (typeof orden["formato_tarjeta"] !== 'undefined') {
                              if (formatos_tarjeta.length>=1){
                                  while((!encuentraformatotarjeta) && (f<formatos_tarjeta.length)){
                                      //  util_sisgein.printLog("formato tarjeta: "+ formatos_tarjeta[f]["id_formatotarjeta"]+" indicef:"+f);
                                        if (orden["formato_tarjeta"]==formatos_tarjeta[f]["id_formatotarjeta"]){
                                            encuentraformatotarjeta=true;
                                            descripcionformatotarjeta=formatos_tarjeta[f]["descripcion_formatotarjeta"];
                                            util_sisgein.printLog("descripcionformatotarjeta: "+ formatos_tarjeta[f]["descripcion_formatotarjeta"]+" encontrado:"+f);
                                        }else {
                                          f++;
                                        }
                                  }
                              }
                              }

                              util_sisgein.printLog("orden diseno tarjeta: "+ orden["diseno_tarjeta"]+" indice:"+x);
                              if (typeof orden["diseno_tarjeta"] !== 'undefined') {
                              if (disenos_tarjeta.length>=1){
                                  while((!encuentradisenotarjeta ) && (d<disenos_tarjeta.length)){
                              //          util_sisgein.printLog("stock diseno tarjeta: "+ disenos_tarjeta[d]["id_disenotarjeta"]+" indiced:"+d);
                                        if (orden["diseno_tarjeta"]==disenos_tarjeta[d]["id_disenotarjeta"])
                                          {
                                            encuentradisenotarjeta=true;
                                            descripciondisenotarjeta=disenos_tarjeta[d]["descripcion_disenotarjeta"];
                                          }else {
                                            d++;
                                          }
                                  }
                              }
                              }

                              orden["descripciontipotarjeta"]=descripciontipotarjeta;
                              orden["descripcionformatotarjeta"]=descripcionformatotarjeta;
                              orden["descripciondisenotarjeta"]=descripciondisenotarjeta;
                              orden["descripcionoficina"]=descripcionoficina;
                              arrayordenes[x]=orden;
                            }








                                  response = {"codigo_mensaje":"01","mensaje" : "Listado exitoso'.","numero_elementos":+body.length,"ordenes":body}

                          } else {
                                  res.status(201);
                                  response = {"codigo_mensaje":"00","mensaje" : "Ningún elemento 'orden'."}
                              }
                }
                res.send(response);
        });
      }

// ordenes
      } else {
        responsediseno = {"codigo_mensaje":"00","mensaje" : "No existe disenos d etarjeta  a procesar"}
        res.status(404);
         res.send(responsediseno);
      }
      }

      });
// Diseno Tarjeta
      } else {
      responseformato = {"codigo_mensaje":"00","mensaje" : "No existe formatos de etarjeta  a procesar"}
      res.status(404);
      res.send(responseformato);
      }
      }

      });


//Formato Tarjeta
      } else {
        responsetipo = {"codigo_mensaje":"00","mensaje" : "No existe tipos d etarjeta  a procesar"}
        res.status(404);
        res.send(responsetipo);
      }
    }

  });

//TIPO TARJETA


    } else {
    responseoficinas = {"codigo_mensaje":"00","mensaje" : "No existe oficinas a listar"}
    res.status(404);
    res.send(responseoficinas);
    }
    }

    });


    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion en el metodo getByFilterOrder","detalle":ex+""});
    }
};

exports.insertOrder = function(req, res) {
  // var nro_orden = req.body.nro_orden;
  // console.log("[insertOrden] nro_orden " + nro_orden);
  var queryStrField = 'f={"_id":0}&';
  var httpClient = requestJSON.createClient(URL_API_MLAB);
  httpClient.get('ordenes?' + queryStrField + API_KEY_MLAB,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"codigo_mensaje":"00","mensaje" : "Error en el servicio al insertar una Orden"}
          res.status(500);
          res.send(response);
      } else {

        var newId=body.length +1;
        var fecha=new Date();
        var fchcreacion=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
        var hrscreacion=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
        util_sisgein.printLog("new Id:"+newId);
        util_sisgein.printLog("fecha:"+fecha);
        util_sisgein.printLog("fchcreacion:"+fchcreacion);
        util_sisgein.printLog("hrscreacion:"+hrscreacion);
        var queryString = 'q={"nro_orden":'  + newId + '}&';

        console.log(" body.length " + body.length);
          let newOrden = {
            "nro_orden" : newId,
            "oficina_solicitante": req.body.oficina_solicitante,
            "tipo_tarjeta": req.body.tipo_tarjeta,
            "formato_tarjeta": req.body.formato_tarjeta,
            "nro_atm_comercio": req.body.nro_atm_comercio,
            "diseno_tarjeta": req.body.diseno_tarjeta,
            "can_tarjetasord": req.body.can_tarjetasord,
            "can_tarjetasrec": 0,
            "situacion_orden": "T",
            "fecha_alta": {"$date": moment(fchcreacion,"DD-MM-YYYY").toISOString()},
            "hora_alta": hrscreacion,
            "usuario_alta": req.body.usuario_alta,
            "fecha_estampacion": "",
            "hora_estampacion": "",
            "usuario_estampacion": "",
            "fecha_anulacion": "",
            "hora_anulacion": "",
            "usuario_anulacion": "",
            "leyenda": req.body.leyenda,
            "usuario_actualizacion": "",
            "terminal_actualizacion": "",
            "timestamp":{"$date": moment(fchcreacion,"DD-MM-YYYY").toISOString()},
            "numero_cajetin":req.body.numero_cajetin
          };
          httpClient.post("ordenes?" + API_KEY_MLAB, newOrden,
              function (err, respuestaMLab, body){
                response = {"codigo_mensaje":"00","mensaje" : "Orden insertada OK.","order":newOrden}
                res.status(200);
                res.send(response);
              }
          );
      }
    });
}

exports.updateOrder = function (req, res) {
    try{
        let nro_orden=req.body.nro_orden;
        let queryString='q={"nro_orden":'+nro_orden+'}&';
        let limFilter='l=1&';

        var httpClient = requestJSON.createClient(URL_API_MLAB);
        httpClient.get('ordenes?'+queryString+limFilter+API_KEY_MLAB,
        function(err, respuestaMLab, body) {
        util_sisgein.printLog('ordenes?'+queryString+limFilter+API_KEY_MLAB);
        if (!err){
              if (body.length>=1){
                    util_sisgein.printLog("tamanio"+body.length);
                    var fecha=new Date();
                    var fchauditoria=fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
                    var hrsauditoria=fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
                    //Actualizo campos dla orden

                      let updatedorder = {
                      "nro_orden" : req.body.nro_orden,
                      "oficina_solicitante": req.body.oficina_solicitante,
                      "tipo_tarjeta": req.body.tipo_tarjeta,
                      "formato_tarjeta": req.body.formato_tarjeta,
                      "nro_atm_comercio": req.body.nro_atm_comercio,
                      "diseno_tarjeta": req.body.diseno_tarjeta,
                      "can_tarjetasord": req.body.can_tarjetasord,
                      "can_tarjetasrec": req.body.can_tarjetasrec,
                      "situacion_orden": req.body.situacion_orden,
                      "fecha_alta": body[0].fecha_alta,
                      "hora_alta": body[0].hora_alta,
                      "usuario_alta": body[0].usuario_alta,
                      "fecha_estampacion": req.body.fecha_estampacion,
                      "hora_estampacion": req.body.hora_estampacion,
                      "usuario_estampacion": req.body.usuario_estampacion,
                      "fecha_anulacion": req.body.fecha_anulacion,
                      "hora_anulacion": req.body.hora_anulacion,
                      "usuario_anulacion": req.body.usuario_anulacion,
                      "leyenda": req.body.leyenda,
                      "usuario_actualizacion": req.body.usuario_actualizacion,
                      "terminal_actualizacion": req.body.terminal_actualizacion,
                      "timestamp":{"$date": moment(fchauditoria,"DD-MM-YYYY").toISOString()},
                      "numero_cajetin":req.body.numero_cajetin
                    };
                    util_sisgein.printLog(updatedorder);
                    util_sisgein.printLog(body[0]._id);
                    httpClient.put('ordenes/' + body[0]._id.$oid + '?' + API_KEY_MLAB, updatedorder,
                    function(errPut, respuestaMLabPut, bodyPut){
                           var response = {};
                           if(errPut) {
                              res.status(400);
                              response = { "mensaje" : "Error actualizando orden."};
                           } else {
                                    res.status(200);
                                    util_sisgein.printLog('bodyPut'+ JSON.stringify(bodyPut));
                                    response =  {"codigo_mensaje":"01","mensaje":"Orden  actualizada satisfactoriamente","ordencalculada":bodyPut};
                           }
                           res.send(response);
                     });
              }else{
                res.status(400);
                res.send({"codigo_mensaje":"00","mensaje":"No se encontró la orden  con el id indicado"});
              }
            }else{
              res.status(400);
              res.send({"codigo_mensaje":"00","mensaje":"Ocurrió un error al actualizar la orden  ","detalle":err.message});
          }
        });
    }catch(ex){
        res.status(500);
        res.send({"codigo_mensaje":"00","mensaje":"Excepcion al llamar al metodo orden.update","detalle":ex+""});
    }
};

exports.deleteOrder = function(req, res) {
  var nro_orden = req.params.id;
  console.log("[deleteOrden] => orden a eliminar " + nro_orden);

  var queryStringID = 'q={"nro_orden":' + nro_orden + '}&';

  var httpClient = requestJSON.createClient(URL_API_MLAB);
  httpClient.get('ordenes?' +  queryStringID + API_KEY_MLAB,
     function(error, respuestaMLab, body){
       if (!error && body.length > 0) {
         var respuesta = body[0];
         httpClient.delete("ordenes/" + respuesta._id.$oid +'?'+ API_KEY_MLAB,
            function(error, respuestaMLab,body){
              res.send({"mensaje":"Orden eliminada OK"});
         });
       }
       else {
         res.status(404).send({"mensaje":"Nro de Orden no valido"});
       }
     });
}
